import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoaderComponent } from '../../layout/bs-component/components/loader/loader.component';
import { ConfirmDialogComponent } from '../../layout/bs-component/components/confirm-dialog/confirm-dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { CommonService } from '../services/common.service';
import { ApiService } from '../services/api.service';
import { OrderService } from '../services/order.service';
import { UserService } from '../services/user.service';
import { LoaderService } from '../services/loader.service';
import { NgSelectModule, NG_SELECT_DEFAULT_CONFIG } from '@ng-select/ng-select';
import { ToastrModule } from 'ngx-toastr';
import { ScrollbarModule } from 'ngx-scrollbar';
import { CustomRendererComponent } from '../../layout/users/user-list/user-list.component';
import { ChangeStatusComponent } from '../../layout/bs-component/components/change-status/change-status.component';
import { AlwaysAuthGuard } from '../guard';
import { CommentComponent } from '../../layout/bs-component/components/comment/comment.component';
import { PictureService } from '../services/picture.service';
import { HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { InvoiceService } from '../services/invoice.service';
import { CommentService } from '../services/comment.service';
import { ViewCommentComponent } from '../../layout/bs-component/components/view-comment/view-comment.component';
import { InvoiceStatusComponent } from '../../layout/bs-component/components/invoice-status/invoice-status.component';

// AoT requires an exported function for factories
export const createTranslateLoader = (http: HttpClient) => {
  /* for development
  return new TranslateHttpLoader(
      http,
      '/start-angular/SB-Admin-BS4-Angular-6/master/dist/assets/i18n/',
      '.json'
  ); */
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    NgSelectModule,
    ScrollbarModule,

    // TranslateModule.forRoot({
    //   loader: {
    //     provide: TranslateLoader,
    //     useFactory: createTranslateLoader,
    //     deps: [HttpClient]
    //   }
    // }),
    ToastrModule.forRoot({
      timeOut: 4000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    })
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    Ng2SmartTableModule,
    NgSelectModule,
    ScrollbarModule,
    TranslateModule,
    ToastrModule,
    LoaderComponent,
    ConfirmDialogComponent,
    ChangeStatusComponent,
    CommentComponent
  ],
  providers: [
    AlwaysAuthGuard,
    ApiService,
    OrderService,
    UserService,
    CommonService,
    PictureService,
    InvoiceService,
    CommentService,
    {
      provide: NG_SELECT_DEFAULT_CONFIG,
      useValue: {
        notFoundText: 'No Match Found'
      }
    }],
  declarations: [LoaderComponent, ConfirmDialogComponent, ViewCommentComponent, CustomRendererComponent,
    ChangeStatusComponent, CommentComponent, InvoiceStatusComponent],
  entryComponents: [ConfirmDialogComponent, CustomRendererComponent, ChangeStatusComponent,
    ViewCommentComponent, CommentComponent, InvoiceStatusComponent]
})
export class SharedModule { }
