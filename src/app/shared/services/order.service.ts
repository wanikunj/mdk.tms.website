import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import 'rxjs/Rx';
import { Order } from '../../model/order.model';

@Injectable()
export class OrderService {

  public uri;
  constructor(private _api: ApiService, private http: HttpClient) { }

  addVendorOrder(data: any): Observable<any> {
    return this._api.apiCaller('post', '/Orders/AddOrder', data);
  }

  getAuctionList() {
    return this._api.apiCaller('get', '/Helper/GetAuctions');
  }

  getMDKGarages() {
    return this._api.apiCaller('get', '/Helper/GetMDKGarages');
  }

  getLocalPorts() {
    return this._api.apiCaller('get', '/Helper/GetLocalPorts');
  }

  getVehicleType() {
    return this._api.apiCaller('get', '/Helper/GetVehicleTypes');
  }

  getTruckCategory() {
    return this._api.apiCaller('get', '/Helper/GetTruckCategory');
  }

  getTruckWeight() {
    return this._api.apiCaller('get', '/Helper/GetTruckWeight');
  }
  getCondition() {
    return this._api.apiCaller('get', '/Helper/GetConditions');
  }
  getVendorName() {
    return this._api.apiCaller('get', '/Helper/GetVendors');
  }
  getDriverUsers() {
    return this._api.apiCaller('get', '/Helper/GetDriverUsers');
  }
  getOrderById(id: any) {
    return this._api.apiCaller('get', '/Orders/GetOrderForUpdate/' + id);
  }
  getOrderForView(id: any) {
    return this._api.apiCaller('get', '/Orders/GetOrderForView/' + id);
  }
  getCarDetailById(id: any) {
    return this._api.apiCaller('get', '/Orders/GetCarDetails/' + id);
  }
  getAllOrder() {
    return this._api.apiCaller('get', '/Orders/GetOrders');
  }
  getManagerPendingItems() {
    return this._api.apiCaller('get', '/Orders/GetManagerPendingItems');
  }
  getVendorPendingItems(id: any) {
    return this._api.apiCaller('get', '/Orders/GetVendorPendingItems/' + id);
  }
  updateOrder(data) {
    return this._api.apiCaller('post', '/Orders/UpdateOrder', data);
  }
  SearchManagerOrders(data) {
    return this._api.apiCaller('post', '/Orders/SearchManagerOrders', data);
  }
  UpdateOrderStatus(data) {
    return this._api.apiCaller('post', '/Orders/UpdateOrderStatus', data);
  }

  getDriverOrderList(data) {
    return this._api.apiCaller('post', '/Orders/GetDriverOrders', data);
  }

  getPortOrderList(data) {
    return this._api.apiCaller('post', '/Orders/GetPortOrders', data);
  }

  getOrderByFilter(data) {
    return this._api.apiCaller('get', '/Helper/GetAuctions/', data);
  }

  getVendorOrderList(data) {
    return this._api.apiCaller('post', '/Orders/VendorOrders', data);
  }
  /**
     * @name getOrders
     * @function get orders
     * @returns list of orders
     * */

  getOrders() {
    const endpoint = this._api.getOrders;
    return this.http.get(endpoint, this._api.getJsonOptions())
      .map(this._api.extractData)
      .catch((error: any) => Observable.throw(typeof error.json === 'function' ? error.json().error : error.message || 'Server error'));
  }

  /**
     * @name addOrders
     * @function add orders
     * */


  addOrders1(data: Order) {
    let data1 = {
      OrderId: 2,
      CustomerName: data.CustomerName,
      CompanyNumber: data.CompanyNumber,
      CompanyMobile: data.CompanyMobile,
      CompanyFax: data.CompanyFax,
      CompanyAddress: data.CompanyAddress,
      AuctionName: data.AuctionName,
      AuctionMembershipNumber: data.AuctionMembershipNumber,
      LotNumber: data.LotNumber,
      CarName: data.CarName,
      ChassisNumber: data.ChassisNumber,
      TrackingDigit: data.TrackingDigit,
      Destination: data.Destination,
      ExportDestination: data.ExportDestination,
      Location: data.Location,
      POSNumber: data.POSNumber,
      PlateNumber: data.PlateNumber,
      Remarks: data.Remarks,
    }
    // let jdata = JSON.stringify(data1);
    return this.saveOrder(data1);
  }
  saveOrder(data: any): Observable<any> {

    return this.apiCaller('post', "", data);

  }
  public apiCaller(type: string, url: string, data?: any): any {

    //this.tost.info("Please wait.......", "Loading");
    this.uri = this._api.addorders;
    return this.post(this.uri, data)

  }
  private post(url: string, data: any): any {
    return this.http.post(url, data, { headers: this.getHeaders() })
  }
  private get(url: string): any {
    return this.http.get(url, { headers: this.getHeaders() })
  }
  private getHeaders(): HttpHeaders {
    let headers = new HttpHeaders();
    headers.append('Access-Control-Allow-Origin', '*');
    return headers;
  }

}
