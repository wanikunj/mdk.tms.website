import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PictureService {

  public uri;
  constructor(private _api: ApiService, private http: HttpClient) { }

  getPictureList(id: any) {
    return this._api.apiCaller('get', '/Orders/GetOrderPictures/' + id);
  }

  addPicture(data: any) {
    return this._api.apiCaller('post', '/Orders/AddOrderPicture', data);
  }

  updatePictureComment(data: any) {
    return this._api.apiCaller('post', '/Orders/UpdateOrderPicture', data);
  }

  getPictureById(id: any) {
    return this._api.apiCaller('get', '/Orders/GetOrderPictureByID/' + id);
  }
}
