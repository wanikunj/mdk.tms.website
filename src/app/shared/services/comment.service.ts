import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  public uri;
  constructor(private _api: ApiService) { }

  addOrderComments(data: any): Observable<any> {
    return this._api.apiCaller('post', '/Orders/AddOrderComments', data);
  }

  getOrderComments(id: any): Observable<any> {
    return this._api.apiCaller('get', '/Orders/GetOrderComments/' + id);
  }
}
