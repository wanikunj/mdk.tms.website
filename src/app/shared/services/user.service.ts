import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  public uri;
  constructor(private _api: ApiService, private http: HttpClient) { }

  addUser(data: any): Observable<any> {
    return this._api.apiCaller('post', '/Users/AddUser', data);
  }
  updateUser(data: any): Observable<any> {
    return this._api.apiCaller('post', '/Users/UpdateUser', data);
  }
  getAllInternalUsers(): Observable<any> {
    return this._api.apiCaller('get', '/Users/GetAllInternalUsers');
  }
  getUserByID(id: any): Observable<any> {
    return this._api.apiCaller('get', '/Users/GetUserByID/' + id);
  }
  getVendors(): Observable<any> {
    return this._api.apiCaller('get', '/Vendor/GetAllVendors');
  }
  getActiveVendors(): Observable<any> {
    return this._api.apiCaller('get', '/Vendor/GetActiveVendors');
  }
  getAllUserTypes(): Observable<any> {
    return this._api.apiCaller('get', '/Users/GetAllUserTypes');
  }
  getAllLocalPorts(): Observable<any> {
    return this._api.apiCaller('get', '/Users/GetAllLocalPorts');
  }
  blockUser(id: any): Observable<any> {
    return this._api.apiCaller('get', '/Users/BlockUser/' + id);
  }
  unblockUser(id: any): Observable<any> {
    return this._api.apiCaller('get', '/Users/UnBlockUser/' + id);
  }
}
