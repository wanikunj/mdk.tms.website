import { Injectable } from '@angular/core';
import { UserLogin } from '../../model/login-user.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GuardService {

  private _currentUser: UserLogin;



  private currentUser: BehaviorSubject<UserLogin> = new BehaviorSubject<UserLogin>(new UserLogin());
  user$: Observable<UserLogin> = this.currentUser.asObservable();



  constructor(private http: HttpClient) { }

  setCurrentUser(user: UserLogin) {
    if (user) {
      this._currentUser = user;
      this.currentUser.next(user);
    }
  }

  logoutCurrentUser() {
    this.currentUser.next(new UserLogin());
  }

  getCurrentUser(): UserLogin {
    return this._currentUser;
  }

  isAdmin(): boolean {
    return this._currentUser.isAdmin;
  }

  isDriver(): boolean {
    return this._currentUser.isDriver;
  }
  isManager(): boolean {
    return this._currentUser.isManager;
  }
}
