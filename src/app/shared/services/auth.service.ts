import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { UserLogin, LoginModal } from '../../model/login-user.model';
import { GuardService } from './guard.service';
import { User } from '../../model/user.model';
import { CommonService } from './common.service';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from './loader.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router, private _loaderService: LoaderService,
    private cookieService: CookieService, private _toasterService: ToastrService,
    private _api: ApiService, private _guard: GuardService, private _commonService: CommonService) { }

  login(username: string, password: string) {
    this.cookieService.delete('currentUser');
    this.cookieService.deleteAll('/');
    this._guard.setCurrentUser(null);
    //  const user: UserLogin = new UserLogin();
    //   user.username = username;
    //   user.password = password;
    //   user.isAdmin = false;
    //   user.isDriver = true;
    //   user.isLocalPortEmployee = false;
    //   user.isManager = false;
    //   this._user.setCurrentUser(user);
    //   localStorage.setItem('currentUser', JSON.stringify(user));
    //   this.cookieService.set('currentUser', JSON.stringify(user));
    //  const body = JSON.stringify({ Email: username, Password: password, TimeOffset: this._commonService.getUserTimeZoneOffSet() });
    //  return user;
    const user = new LoginModal();
    user.Email = username;
    user.Password = password;
    user.TimeOffset = this._commonService.getUserTimeZoneOffSet();
    return this.onLogin(user)
      .map((users: UserLogin) => {
        if (!users) {
          console.log('Expected a User, received => ', users);
        }
        if (users && users.Token) {
          const min = 80;
          const expireTime = (1 * min) / 24;
          this.cookieService.set('currentUser', JSON.stringify(users), expireTime);
          // this.cookieService.set('token', users.Token, expireTime);
          //  localStorage.setItem('currentUser', JSON.stringify(users));
          this._guard.setCurrentUser(users);
        }
        return users;

      }).catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  logout() {
    this._loaderService.show();
    const user = this._guard.getCurrentUser();
    const body = new LoginModal();
    body.UserID = user.User.UserId;
    body.Token = user.Token;
    this.logUserOut(body).subscribe(data => {
      if (data) {
        this._toasterService.success('User Logged Out Successfully!');
        this.cookieService.delete('currentUser');
        this.cookieService.deleteAll('/');
        //  this._guard.logoutCurrentUser();
        this.router.navigate(['/login']);
      }
      this._loaderService.hide();
    }, error => {
      this._loaderService.hide();
    });
    this._loaderService.hide();
  }

  removeUser() {
    this.cookieService.delete('currentUser');
    //  this._guard.logoutCurrentUser();
  }

  logUserOut(data: any) {
    return this.onLogout(data)
      .map(response => response || {})
      .catch((error: any) => Observable.throw(error.json().error || 'Server error'));
  }

  onLogout(data: any): Observable<any> {
    return this._api.apiCaller('post', '/Users/Signout', data);
  }

  onLogin(data: any): Observable<any> {
    return this._api.apiCaller('post', '/Users/Signin', data);
  }

}
