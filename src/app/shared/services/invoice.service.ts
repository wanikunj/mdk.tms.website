import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ApiService } from './api.service';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  public uri;
  constructor(private _api: ApiService, private http: HttpClient) { }

  addInvoice(data: any): Observable<any> {
    return this._api.apiCaller('post', '/Invoice/CreateInvoice', data);
  }
  getInvoiceOrder(data): Observable<any> {
    return this._api.apiCaller('post', '/Invoice/InvoiceOrders', data);
  }
  getVendorInvoices(id: any): Observable<any> {
    return this._api.apiCaller('get', '/Invoice/GetVendorInvoices/' + id);
  }
  getInvoiceById(id: any) {
    return this._api.apiCaller('get', '/Invoice/GetVendorInvoiceByID/ ' + id);
  }
  updateInvoiceStatus(data) {
    return this._api.apiCaller('post', '/Invoice/UpdateInvoiceStatus ', data);
  }
  deleteInvoice(id) {
    return this._api.apiCaller('get', '/Invoice/DeleteInvoice/ ' + id);
  }
}
