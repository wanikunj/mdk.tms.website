import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import 'rxjs/Rx';

@Injectable({
  providedIn: 'root'
})
export class VendorService {

  public uri;
  constructor(private _api: ApiService, private http: HttpClient) { }

  getAllVendors(): Observable<any> {
    return this._api.apiCaller("get", "/Vendor/GetAllVendors");
  }
  addVendor(data: any): Observable<any> {
    return this._api.apiCaller("post", "/Vendor/AddVendor", data);
  }
  updateVendor(data: any): Observable<any> {
    return this._api.apiCaller("post", "/Vendor/UpdateVendor", data);
  }
  getVendorByID(id: any): Observable<any> {
    return this._api.apiCaller("get", "/Vendor/GetVendorByID/" + id);
  }
  blockVendor(id: any): Observable<any> {
    return this._api.apiCaller("get", "/Vendor/BlockVendor/" + id);
  }
  unblockVendor(id: any): Observable<any> {
    return this._api.apiCaller("get", "/Vendor/UnBlockVendor/" + id);
  }
}
