import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class ManagerGuard implements CanActivate {

    constructor(private router: Router, private cookieService: CookieService) {
    }

    canActivate() {
        const user = JSON.parse(this.cookieService.get('currentUser'));
        const type = user.User.UserType;
        if (type === 'Admin' || type === 'Manager') {
            return true;
        }
        this.router.navigate(['/dashboard']);
        return false;

    }

}
