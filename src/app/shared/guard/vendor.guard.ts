import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class VendorGuard implements CanActivate {

    constructor(private router: Router, private cookieService: CookieService) {
    }

    canActivate() {

        const user = JSON.parse(this.cookieService.get('currentUser'));
        if (user.User != null) {
            const type = user.User.UserType;
            if (type === 'Vendor') {
                return true;
            }
        }
        this.router.navigate(['/dashboard']);
        return false;

    }

}
