import { Injectable } from '@angular/core';
import { Router, CanActivate} from '@angular/router';
import { CookieService } from 'ngx-cookie-service';

@Injectable()
export class LocalPortGuard implements CanActivate {

    constructor(private router: Router, private cookieService: CookieService) {
    }

    canActivate() {

        const user = JSON.parse(this.cookieService.get('currentUser'));
        const type = user.User.UserType;
        if (type === 'Local Port Employee') {
            return true;
        }
        this.router.navigate(['/dashboard']);
        return false;

    }

}
