import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { Router } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { GuardService } from '../services/guard.service';
import { UserLogin } from '../../model/login-user.model';

@Injectable()
export class AlwaysAuthGuard implements CanActivate {
    constructor(private router: Router, private cookieService: CookieService, private _guardService: GuardService) { }

    canActivate() {
        const users = this._guardService.getCurrentUser();
        if (users) {
            return true;
        } else {
            if (this.cookieService.get('currentUser')) {
                const user: UserLogin = JSON.parse(this.cookieService.get('currentUser'));
                this._guardService.setCurrentUser(user);
                return true;

            } else {
                this.router.navigate(['/login']);
                return false;
            }
        }
    }
}
