import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { routerTransition } from '../router.animations';
import { UserLogin } from '../model/login-user.model';
import { AuthService } from '../shared/services/auth.service';
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from '../shared/services/loader.service';
import { GuardService } from '../shared/services/guard.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss'],
    animations: [routerTransition()]
})
export class LoginComponent implements OnInit {
    user: UserLogin;
    constructor(public router: Router, private _authService: AuthService,
        private _toasterService: ToastrService, private _loaderService: LoaderService, private _guard: GuardService) {
        this.user = new UserLogin();
    }

    ngOnInit() { }

    onSubmit(form: NgForm) {
        localStorage.setItem('isLoggedin', 'true');
        // const us = this._authService.login(this.user.username, this.user.password);
        // form.reset();
        //  us.isDriver = true;
        // if (us) {
        //     this.router.navigate(['/dashboard']);
        // }
        this._loaderService.show();
        this._authService.login(this.user.Email, this.user.Password).subscribe(
            data => {
                form.reset();
                this._loaderService.hide();
                if (data == null) {
                    this.router.navigate(['/login']);
                    this._toasterService.show('User not found');
                } else {
                    if (data.User != null) {
                        if (data.User.UserType === 'Admin' || data.User.UserType === 'Manager') {
                            this.router.navigate(['/dashboard']);
                        } else if (data.User.UserType === 'Driver') {
                            this.router.navigate(['/dashboard/driver-dashboard']);
                        } else if (data.User.UserType === 'Local Port Employee') {
                            this.router.navigate(['/dashboard/port-employee']);
                        } else if (data.User.UserType === 'Vendor') {
                            this.router.navigate(['/dashboard/vendor-dashboard']);
                        }
                    } else {
                        this._toasterService.show('User not found');
                    }

                }
                this._loaderService.hide();
            },
            error => {
                this._toasterService.error('User login failed!!');
                this._loaderService.hide();
            });
    }
}
