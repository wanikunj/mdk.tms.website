import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    constructor(private translate: TranslateService) {
        // this.translate.addLangs(['en', 'ja']);
        // this.translate.setDefaultLang('en');

        // Use a language
        // this.translate.use('en');
        // const browserLang = translate.getBrowserLang();
        // translate.use(browserLang.match(/en|ja/) ? browserLang : 'en');
    }

    ngOnInit() {
    }
}
