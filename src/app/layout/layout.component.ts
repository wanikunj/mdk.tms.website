import { Component, OnInit } from '@angular/core';
import { LoaderService } from '../shared/services/loader.service';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';

@Component({
    selector: 'app-layout',
    templateUrl: './layout.component.html',
    styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnInit {

    collapedSideBar: boolean;

    constructor(private _loaderService: LoaderService, private router: Router) {
        // router.events.subscribe(
        //     (event) => {
        //         if (event instanceof NavigationStart) {
        //             console.log('layout start ', this._loaderService.loaderState);
        //             this._loaderService.show();
        //         }
        //         if (event instanceof NavigationEnd) {
        //             console.log('layout end', this._loaderService.loaderState);
        //             this._loaderService.hide();
        //         }
        //     });

    }

    ngOnInit() { }

    receiveCollapsed($event) {
        this.collapedSideBar = $event;
    }
}
