import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { Router } from '@angular/router';
import { ViewCell, LocalDataSource } from 'ng2-smart-table';
import { UserService } from '../../../shared/services/user.service';
import { LoaderService } from '../../../shared/services/loader.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { User } from '../../../model/user.model';

import { OrderService } from '../../../shared/services/order.service';
import { OrderList } from '../../../model/order-grid.model';

@Component({
  selector: 'app-orders-list',
  templateUrl: './orders-list.component.html',
  styleUrls: ['./orders-list.component.scss'],
  animations: [routerTransition()]
})
export class OrdersListComponent implements OnInit {

  usersList: User[];
  orderList: OrderList[];
  orderListCopy: OrderList[];
  userListCopy: User[];
  selectedUserType = '2';
  source = new LocalDataSource(); // add a property to the component
  settings = {
    actions: false,
    columns: {
      OrderId: {
        title: 'OrderId',
        // type: 'custom',
        // valuePrepareFunction: (cell, usersList) => {
        //   return usersList.FirstName + ' ' + usersList.LastName;
        // },
        // renderComponent: OrderListCustomRendererComponent,
        filter: false
      },
      VendorName: {
        title: 'Vendor Name',
        filter: false
      },
      OrderStatus: {
        title: 'Order Status',
        filter: false
      },
      OrderDate: {
        title: 'Order Date',
        filter: false,
      },
      // IsActive: {
      //   title: 'Action',
      //   type: 'custom',
      //   renderComponent: OrderListCustomRendererComponent,
      //   filter: false
      // },
    },
    pager: {
      display: true,
      perPage: 20,
    },
    hideSubHeader: true,
    sort: true,
  };
  constructor(
    private _orderService: OrderService,
    private _router: Router,
    private _loaderService: LoaderService,
    private _modalService: NgbModal
  ) {
    this.orderList = [];
    this.orderListCopy = [];
  }

  ngOnInit() {
    this.getOrdersList();
  }

  getOrdersList(): any {
    this._loaderService.show();
    this._orderService.getAllOrder().subscribe(
      data => {
        this._loaderService.hide();
        if (data) {
          this.orderList = data;
          this.source = new LocalDataSource(this.orderList);
        }
      }, error => {
        this._loaderService.hide();
      });
  }

  onSearch(query: string = '') {
    if (query) {
      this.source.setFilter([
        {
          field: 'OrderId',
          search: query
        },
        {
          field: 'VendorName',
          search: query
        },
        {
          field: 'OrderStatus',
          search: query
        },
        {
          field: 'AuctionName',
          search: query
        }, {
          field: 'OrderDate',
          search: query
        }
      ], false, true);
    } else {
      this.source = new LocalDataSource(this.orderList);
    }
  }

  // onSelectUserType() {
  //   this._loaderService.show();
  //   this.userListCopy = [];
  //   let flag = false;
  //   if (this.selectedUserType === '') {
  //     this.source = new LocalDataSource(this.usersList);
  //     this._loaderService.hide();
  //   } else {
  //     this.usersList.forEach((user, index) => {
  //       if (user.UserTypeID == this.selectedUserType) {
  //         this.userListCopy.push(user);
  //       }
  //       if (index === this.usersList.length - 1) {
  //         flag = true;
  //       }
  //     });
  //     if (flag) {
  //       this.source = new LocalDataSource(this.userListCopy);
  //       this._loaderService.hide();
  //     }
  //   }
  // }
}
@Component({
  template: `
  <a class="btn btn-link" (click)="navigateToSomeRoute()">{{title}}</a>
  `,
  styleUrls: ['./orders-list.component.scss'],
})
export class OrderListCustomRendererComponent implements ViewCell, OnInit {
  value: string | number;
  rowData: any;
  title: any;
  @Output() save: EventEmitter<any> = new EventEmitter();
  constructor(private router: Router) {
  }

  renderValue: string;


  ngOnInit() {
    if (typeof (this.value) === 'boolean') {
      this.title = 'Edit Order';
    }
  }
  navigateToSomeRoute() {
    if (typeof (this.value) === 'boolean') {
      this.router.navigate(['/neworders/edit-order']);
    }
  }
}
