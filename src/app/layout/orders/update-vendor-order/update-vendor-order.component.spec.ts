import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateVendorOrderComponent } from './update-vendor-order.component';

describe('UpdateVendorOrderComponent', () => {
  let component: UpdateVendorOrderComponent;
  let fixture: ComponentFixture<UpdateVendorOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateVendorOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateVendorOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
