import { Component, OnInit, OnDestroy } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderService } from '../../../shared/services/order.service';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from '../../../shared/services/loader.service';
import { HelperClass } from '../../../model/vendor-order.model';
import { NgForm } from '@angular/forms';
import { UpdateOrderDetails } from '../../../model/update-order.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Vendor } from '../../../model/vendor.model';
import { CommonService } from '../../../shared/services/common.service';
import { Subscription } from 'rxjs';
import { UserLogin } from '../../../model/login-user.model';
import { GuardService } from '../../../shared/services/guard.service';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';
import { ViewPicturesComponent } from '../view-pictures/view-pictures.component';
import { CommentService } from '../../../shared/services/comment.service';
import { ViewCommentComponent } from '../../bs-component/components/view-comment/view-comment.component';

@Component({
  selector: 'app-update-vendor-order',
  templateUrl: './update-vendor-order.component.html',
  styleUrls: ['./update-vendor-order.component.scss'],
  animations: [routerTransition()]
})
export class UpdateVendorOrderComponent implements OnInit, OnDestroy {

  orderId: any;
  orderObj: any;
  updateOrderObj: UpdateOrderDetails;
  vendorDetail: Vendor;
  vehicleTypeList: HelperClass[] = [];
  truckCategoryList: HelperClass[] = [];
  truckWeightList: HelperClass[] = [];
  conditionList: HelperClass[] = [];
  driverUserList: HelperClass[] = [];
  orderStatusList = [
    { 'ID': '1', 'Value': 'Pending' },
    { 'ID': '2', 'Value': 'Created' },
    { 'ID': '3', 'Value': 'Picked' },
    { 'ID': '4', 'Value': 'Delivered' },
    { 'ID': '5', 'Value': 'Received at port' }];
  user$: Subscription;
  currentUser: UserLogin;
  userId: any;
  updateDate = [];

  constructor(private _orderService: OrderService, private _toasterService: ToastrService,
    private _loaderService: LoaderService,
    private _commonService: CommonService,
    private router: Router, private _modal: NgbModal,
    private _commentService: CommentService,
    private _guard: GuardService,
    private route: ActivatedRoute) {
    this.updateOrderObj = new UpdateOrderDetails();
    this.vendorDetail = new Vendor();
    console.log('obj', this.updateOrderObj);
  }

  ngOnInit() {
    this.orderId = this.route.snapshot.paramMap.get('id');
    this.user$ = this._guard.user$.subscribe(user => {
      this.currentUser = user;
      if (this.currentUser.User != undefined || this.currentUser.User !== null) {
        this.userId = this.currentUser.User.UserId;
      }
    });
    this.getTruckCategory();
    this.getCondition();
    this.getTruckWeight();
    this.getVehicleType();
    this.getDriverUsers();
    this.getOrderById(this.orderId);
  }

  ngOnDestroy() {
    this.user$.unsubscribe();
  }
  getOrderById(orderId: any) {
    console.log('id', orderId);
    this._loaderService.show();
    this._orderService.getOrderById(orderId).subscribe((data) => {
      if (data) {
        this.updateOrderObj = data;
        this.vendorDetail = this.updateOrderObj.Vendor;
        this.updateDate = this.updateOrderObj.UpdatedDate.split(' ');
        console.log('order', this.orderObj);
        this._loaderService.hide();
      } else {
        this._loaderService.hide();
      }
    }, error => {
      this._loaderService.hide();
    });
  }
  getTruckCategory() {
    this._loaderService.show();
    this._orderService.getTruckCategory().subscribe((data) => {
      this.truckCategoryList = data;
      this._loaderService.hide();
    }, error => {
      this._loaderService.hide();
    });
  }
  getVehicleType() {
    this._loaderService.show();
    this._orderService.getVehicleType().subscribe((data) => {
      this.vehicleTypeList = data;
      this._loaderService.hide();
    }, error => {
      this._loaderService.hide();
    });
  }

  getTruckWeight() {
    this._loaderService.show();
    this._orderService.getTruckWeight().subscribe((data) => {
      this.truckWeightList = data;
      this._loaderService.hide();
    }, error => {
      this._loaderService.hide();
    });
  }
  getCondition() {
    this._loaderService.show();
    this._orderService.getCondition().subscribe((data) => {
      this.conditionList = data;
      this._loaderService.hide();
    }, error => {
      this._loaderService.hide();
    });
  }

  getDriverUsers() {
    this._loaderService.show();
    this._orderService.getDriverUsers().subscribe((data) => {
      this.driverUserList = data;
      this._loaderService.hide();
    }, error => {
      this._loaderService.hide();
    });
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  onSubmit(form: NgForm) {
    this.updateOrderObj.TimeOffSet = this._commonService.getUserTimeZoneOffSet();
    console.log('update', this.updateOrderObj);
    this.updateOrderObj.Vendor = this.vendorDetail;
    this.updateOrderObj.UpdatedBy = this.userId;
    this.updateOrderObj.UpdatedDate = new Date(Date.now());
    if (this.updateOrderObj.ConditionID == 1 && this.updateOrderObj.VehicleTypeID != 3) {
      this.updateOrderObj.Price = '';
      this.updateOrderObj.Message = '';
    }
    this._loaderService.show();
    this._orderService.updateOrder(this.updateOrderObj).subscribe((data) => {
      this._loaderService.hide();
      if (data === true) {
        this._toasterService.success('Order Updated Successfully!!');
        this.router.navigate(['/dashboard/manager-dashboard']);
      } else {
        this._toasterService.error('Order not updated Successfully!!');
      }
    }, error => {
      this._loaderService.hide();
      this._toasterService.error('Server Error Occurred');
    });
  }

  onViewPictures() {
    const options: NgbModalOptions = {
      size: 'lg',
    };
    const modalRef = this._modal.open(ViewPicturesComponent, options);
    modalRef.componentInstance.orderId = this.orderId;
  }
  onViewComments() {
    const options: NgbModalOptions = {
      size: 'lg',
    };
    const modalRef = this._modal.open(ViewCommentComponent, options);
    modalRef.componentInstance.orderId = this.orderId;
  }
}
