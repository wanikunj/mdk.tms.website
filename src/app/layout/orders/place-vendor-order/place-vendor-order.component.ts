import { Component, OnInit, OnDestroy } from '@angular/core';
import { VendorOrder, VendorOrderDetail, Auction, DestinationLocationId, MDKGarage, LocalPort, HelperClass } from '../../../model/vendor-order.model';
import { routerTransition } from '../../../router.animations';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { OrderService } from '../../../shared/services/order.service';
import { LoaderService } from '../../../shared/services/loader.service';
import { ToastrService } from 'ngx-toastr';
import { toInteger } from '@ng-bootstrap/ng-bootstrap/util/util';
import { CommonService } from '../../../shared/services/common.service';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { element } from '@angular/core/src/render3/instructions';
import { Subscription } from 'rxjs';
import { UserLogin } from '../../../model/login-user.model';
import { GuardService } from '../../../shared/services/guard.service';

@Component({
  selector: 'app-place-vendor-order',
  templateUrl: './place-vendor-order.component.html',
  styleUrls: ['./place-vendor-order.component.scss'],
  animations: [routerTransition()]
})
export class PlaceVendorOrderComponent implements OnInit, OnDestroy {

  vendorOrder: VendorOrder;
  deleteRowFlag = false;
  auctionLocation = '';
  userName = '';
  Auction: Auction[];
  AuctionCopy: Auction[];
  MDKGarageList: MDKGarage[];
  LocalPort: LocalPort[];
  AuctionHouseList: Auction[];
  destinationLocationId: DestinationLocationId[];
  model: NgbDateStruct;
  date: { year: number, month: number };
  now = new Date();
  initialLocalPortFlag: Boolean = true;
  DestinationLocation = [
    { 'value': 1, 'name': 'Local Port' },
    { 'value': 2, 'name': 'MDK Garage' },
    { 'value': 3, 'name': 'Auction House' }];
  user$: Subscription;
  currentUser: UserLogin;
  userId: any;
  OrderDetail;
  userTypeId;
  vendorName;
  VendorID = '';
  vendorList: HelperClass[];
  constructor(
    private router: Router,
    private _guard: GuardService,
    private _orderService: OrderService,
    private _loaderService: LoaderService,
    private _commonService: CommonService,
    private _toasterService: ToastrService
  ) {
    this.vendorOrder = new VendorOrder();
    this.Auction = [];
    this.MDKGarageList = [];
    this.LocalPort = [];
    this.AuctionCopy = [];
    this.destinationLocationId = [];
    this.AuctionHouseList = [];
  }


  ngOnInit() {
    this.user$ = this._guard.user$.subscribe(user => {
      this.currentUser = user;
      if (this.currentUser.User != undefined || this.currentUser.User !== null) {
        this.userId = this.currentUser.User.UserId;
        this.userTypeId = this.currentUser.User.UserTypeID;
        if (this.userTypeId == 5) {
          if (this.currentUser.Vendors != null) {
            this.vendorName = this.currentUser.Vendors.VendorName;
            this.VendorID = this.currentUser.Vendors.VendorID;
          }
        } else if (this.userTypeId == 1 || this.userTypeId == 2) {
          this.VendorID = '';
          this.getVendorList();
        }
        this.userName = this.currentUser.User.FirstName + ' ' + this.currentUser.User.LastName;
      }
    });
    this.getAuctions();
    this.vendorOrder.OrderDetails = [];
    const OrderDetail = new VendorOrderDetail();
    this.vendorOrder.OrderDetails.push(OrderDetail);
    this.model = { year: this.now.getFullYear(), month: this.now.getMonth() + 1, day: this.now.getDate() };
    this.vendorOrder.AuctionDate = this.model;
    this.destinationLocationId = [];
    const destinationObject = { 'index': 0, 'value': '1' };
    this.destinationLocationId.push(destinationObject);
    this.getLocalPort();
  }
  ngOnDestroy() {
    this.user$.unsubscribe();
  }
  getAuctions() {
    this._orderService.getAuctionList().subscribe((data) => {
      this._loaderService.hide();
      if (data) {
        this.Auction = data;
        this.AuctionCopy = [...this.Auction];
        this.setAuctionLocation();
        console.log('data', this.Auction);
      }
    }, error => {
      this._loaderService.hide();
    });
  }
  getVendorList(): any {
    this._loaderService.show();
    this._orderService.getVendorName().subscribe(
      data => {
        this._loaderService.hide();
        if (data) {
          this.vendorList = data;
        }
      }, error => {
        this._loaderService.hide();
      });
  }
  setAuctionLocation() {
    this.Auction.forEach((ele) => {
      if (this.vendorOrder.AuctionID == ele.AuctionID) {
        this.auctionLocation = ele.AuctionHouseAddress;
      }
    });
  }

  getLocalPort() {
    this._orderService.getLocalPorts().subscribe((data) => {
      this.LocalPort = data;
      this._loaderService.hide();
    }, error => {
      this._loaderService.hide();
    });
  }

  getMDKGarage() {
    this._orderService.getMDKGarages().subscribe((data) => {
      this.MDKGarageList = data;
      this._loaderService.hide();
    }, error => {
      this._loaderService.hide();
    });
  }
  onSelectDestination(index, value) {
    let flag = false;
    this.initialLocalPortFlag = false;
    this.destinationLocationId.forEach((element) => {
      if (index == element.index) {
        element.value = value;
        flag = true;
      }
    });
    if (!flag) {
      this.destinationLocationId[index].index = index;
      this.destinationLocationId[index].value = value;
    }
    if (value == 1) {
      this._loaderService.show();
      this.vendorOrder.OrderDetails[index].Dest_MDKGarageID = null;
      this.vendorOrder.OrderDetails[index].Dest_AuctionID = null;
      this.vendorOrder.OrderDetails[index].Dest_LocalPortID = 1;
      this._orderService.getLocalPorts().subscribe((data) => {
        this.LocalPort = data;
        this._loaderService.hide();
      });
    } else if (value == 2) {
      this.vendorOrder.OrderDetails[index].Dest_LocalPortID = null;
      this.vendorOrder.OrderDetails[index].Dest_AuctionID = null;
      this.vendorOrder.OrderDetails[index].Dest_MDKGarageID = 1;
      if (!this.MDKGarageList.length) {
        this.getMDKGarage();
      } else {
        this._loaderService.hide();
      }
    } else {
      this.vendorOrder.OrderDetails[index].Dest_AuctionID = 1;
      this.vendorOrder.OrderDetails[index].Dest_MDKGarageID = null;
      this.vendorOrder.OrderDetails[index].Dest_LocalPortID = null;
      this.AuctionHouseList = this.Auction;
    }
  }


  onAddRow() {
    //  this.initialLocalPortFlag = false;
    const Order = new VendorOrderDetail();
    this.vendorOrder.OrderDetails.push(Order);
    const destinationObject = { 'index': this.destinationLocationId.length, 'value': '1' };
    this.destinationLocationId.push(destinationObject);
    this.deleteRowFlag = false;
  }

  onRemoveRow(index: any) {
    if (this.vendorOrder.OrderDetails.length == 1) {
      this.deleteRowFlag = true;
    } else {
      this.vendorOrder.OrderDetails.splice(index, 1);
    }
  }
  onDashboard() {
    this.router.navigate(['/dashboard']);
  }

  onSubmit(form: NgForm) {
    this._loaderService.show();
    this.vendorOrder.AuctionDateValue = this.vendorOrder.AuctionDate['month']
      + '-' + this.vendorOrder.AuctionDate['day']
      + '-' + this.vendorOrder.AuctionDate['year'];
    this.vendorOrder.TimeOffSet = this._commonService.getUserTimeZoneOffSet();
    this.vendorOrder.CreatedBy = this.currentUser.User.UserId;
    this.vendorOrder.UpdatedBy = this.currentUser.User.UserId;
    this.vendorOrder.VendorID = this.VendorID;
    this._orderService.addVendorOrder(this.vendorOrder).subscribe(
      data => {
        if (data) {
          this._loaderService.hide();
          if (data > 0) {
            // const elem = document.getElementById('btnReset');
            // elem.click();
            this.onReset(form);
            this._toasterService.success('Order Added Successfully!!');
          } else {
            this._toasterService.error('Order Not Added');
          }
        }
      }, error => {
        this._loaderService.hide();
        this._toasterService.error('Server Error Occurred');
      });
  }
  onSelectAuctionName(value) {
    this.Auction.forEach((element) => {
      if (value == element.AuctionID) {
        this.auctionLocation = element.AuctionHouseAddress;
      }
    });
  }

  onSelectAddress(index, value) {
    this.destinationLocationId.forEach((element) => {
      if (element.index === index && element.value == 1) {
        this.vendorOrder.OrderDetails[index].Dest_LocalPortID = toInteger(value);
      } else if (element.index === index && element.value == 2) {
        this.vendorOrder.OrderDetails[index].Dest_MDKGarageID = toInteger(value);
      } else if (element.index === index && element.value == 3) {
        this.vendorOrder.OrderDetails[index].Dest_AuctionID = toInteger(value);
      }
    });
  }
  onReset(form: NgForm) {
    this.initialLocalPortFlag = true;
    this._loaderService.show();
    form.resetForm();
    this.getAuctions();
    this.destinationLocationId = [];
    this.vendorOrder = new VendorOrder();
    this.Auction = [];
    this.MDKGarageList = [];
    this.LocalPort = [];
    this.destinationLocationId = [];
    this.AuctionHouseList = [];
    this.ngOnInit();
  }
}

