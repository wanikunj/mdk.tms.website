import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceVendorOrderComponent } from './place-vendor-order.component';

describe('PlaceVendorOrderComponent', () => {
  let component: PlaceVendorOrderComponent;
  let fixture: ComponentFixture<PlaceVendorOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceVendorOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceVendorOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
