import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderService } from '../../../shared/services/order.service';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from '../../../shared/services/loader.service';
import { CommonService } from '../../../shared/services/common.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UpdateOrderDetails } from '../../../model/update-order.model';
import { Vendor } from '../../../model/vendor.model';
import { OrderDetail } from '../../../model/driver.model';
@Component({
  selector: 'app-car-details',
  templateUrl: './car-details.component.html',
  styleUrls: ['./car-details.component.scss']
})
export class CarDetailsComponent implements OnInit {

  orderId: any;
  orderDetailObj: OrderDetail;
  vendor: Vendor;
  constructor(private _orderService: OrderService, private _toasterService: ToastrService,
    private _loaderService: LoaderService,
    private router: Router,
    private route: ActivatedRoute) {
    this.orderDetailObj = new OrderDetail();
    this.vendor = new Vendor();
  }

  ngOnInit() {
    this._loaderService.show();
    this.orderId = this.route.snapshot.paramMap.get('id');
    this.getOrderById(this.orderId);
  }
  getOrderById(orderId: any) {
    this._loaderService.show();
    this._orderService.getCarDetailById(orderId).subscribe((data) => {
      if (data) {
        this.orderDetailObj = data;
        this.vendor = this.orderDetailObj.Vendor;
        console.log('order detail', this.orderDetailObj);
        this._loaderService.hide();
      } else {
        this._loaderService.hide();
      }
    }, error => {
      this._toasterService.error('Server error Occurred!!');
      this._loaderService.hide();
    });
  }

}
