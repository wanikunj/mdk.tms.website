import { Component, OnInit, Input } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { PictureModel, VehicleModel } from '../../../model/picture.model';
import { LoaderService } from '../../../shared/services/loader.service';
import { PictureService } from '../../../shared/services/picture.service';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { environment } from '../../../../environments/environment';
import { NgTypeToSearchTemplateDirective } from '@ng-select/ng-select/ng-select/ng-templates.directive';

@Component({
  selector: 'app-view-pictures',
  templateUrl: './view-pictures.component.html',
  styleUrls: ['./view-pictures.component.scss'],
  animations: [routerTransition()]
})
export class ViewPicturesComponent implements OnInit {

  imageList: PictureModel[] = [];
  vehicleObj: VehicleModel;
  apiUrl: string;
  color = ['primary', 'warning', 'success', 'info'];
  bgClass = [];
  @Input() orderId: any;
  id: any;
  public screenOrientation: any;
  constructor(private _loaderService: LoaderService,
    private _pictureServie: PictureService,
    private route: ActivatedRoute,
    public activeModal: NgbActiveModal) {
    this.vehicleObj = new VehicleModel();
    this.apiUrl = environment.imagePath;
  }

  ngOnInit() {
    this.id = this.orderId;
    this.getPictureList();
  }
  getPictureList() {
    this._loaderService.show();
    this._pictureServie.getPictureList(this.id).subscribe((data) => {
      if (data) {
        this.imageList = data.Pictures;
        let i = 0;
        this.imageList.forEach((ele, index) => {
          if (i < 4) {
            this.bgClass.push(this.color[i]);
            i = i + 1;
          } else {
            i = 0;
            this.bgClass.push(this.color[i]);
            i = i + 1;
          }
        });
        this.vehicleObj = data.Vehicle;
        this._loaderService.hide();
      }
      this._loaderService.hide();
    }, error => {
      this._loaderService.hide();
    });
  }
}
