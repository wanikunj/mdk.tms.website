import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPicturesComponent } from './view-pictures.component';

describe('ViewPicturesComponent', () => {
  let component: ViewPicturesComponent;
  let fixture: ComponentFixture<ViewPicturesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPicturesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPicturesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
