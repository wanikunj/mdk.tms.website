import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderService } from '../../../shared/services/order.service';
import { LoaderService } from '../../../shared/services/loader.service';
import { HelperClass } from '../../../model/vendor-order.model';
import { UpdateOrderDetails } from '../../../model/update-order.model';
import { ActivatedRoute, Router } from '@angular/router';
import { Vendor } from '../../../model/vendor.model';
import { NgbModalOptions, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ViewPicturesComponent } from '../view-pictures/view-pictures.component';
import { ViewCommentComponent } from '../../bs-component/components/view-comment/view-comment.component';

@Component({
  selector: 'app-view-vendor-order',
  templateUrl: './view-vendor-order.component.html',
  styleUrls: ['./view-vendor-order.component.scss'],
  animations: [routerTransition()]
})
export class ViewVendorOrderComponent implements OnInit {

  orderId: any;
  orderObj: any;
  updateOrderObj: UpdateOrderDetails;
  vendorDetail: Vendor;
  vehicleTypeList: HelperClass[] = [];
  truckCategoryList: HelperClass[] = [];
  truckWeightList: HelperClass[] = [];
  conditionList: HelperClass[] = [];
  driverUserList: HelperClass[] = [];
  updateDate = [];
  orderStatusList = [
    { 'ID': '1', 'Value': 'Pending' },
    { 'ID': '2', 'Value': 'Created' },
    { 'ID': '3', 'Value': 'Picked' },
    { 'ID': '4', 'Value': 'Dropped' },
    { 'ID': '5', 'Value': 'Delivered' }];

  constructor(private _orderService: OrderService,
    private _loaderService: LoaderService,
    private _router: Router,
    private _modal: NgbModal,
    private route: ActivatedRoute) {
    this.updateOrderObj = new UpdateOrderDetails();
    this.vendorDetail = new Vendor();
  }

  ngOnInit() {
    this.orderId = this.route.snapshot.paramMap.get('id');
    this.getTruckCategory();
    this.getCondition();
    this.getTruckWeight();
    this.getVehicleType();
    this.getDriverUsers();
    this.getOrderForViewById(this.orderId);
  }

  getOrderForViewById(orderId: any) {
    this._loaderService.show();
    this._orderService.getOrderForView(orderId).subscribe((data) => {
      if (data) {
        this.updateOrderObj = data;
        this.vendorDetail = this.updateOrderObj.Vendor;
        this.updateDate = this.updateOrderObj.UpdatedDate.split(' ');
        this._loaderService.hide();
      } else {
        this._loaderService.hide();
      }
    }, error => {
      this._loaderService.hide();
    });
  }
  getTruckCategory() {
    this._loaderService.show();
    this._orderService.getTruckCategory().subscribe((data) => {
      this.truckCategoryList = data;
      this._loaderService.hide();
    }, error => {
      this._loaderService.hide();
    });
  }
  getVehicleType() {
    this._loaderService.show();
    this._orderService.getVehicleType().subscribe((data) => {
      this.vehicleTypeList = data;
      this._loaderService.hide();
    }, error => {
      this._loaderService.hide();
    });
  }

  getTruckWeight() {
    this._loaderService.show();
    this._orderService.getTruckWeight().subscribe((data) => {
      this.truckWeightList = data;
      this._loaderService.hide();
    }, error => {
      this._loaderService.hide();
    });
  }
  getCondition() {
    this._loaderService.show();
    this._orderService.getCondition().subscribe((data) => {
      this.conditionList = data;
      this._loaderService.hide();
    }, error => {
      this._loaderService.hide();
    });
  }

  getDriverUsers() {
    this._loaderService.show();
    this._orderService.getDriverUsers().subscribe((data) => {
      this.driverUserList = data;
      this._loaderService.hide();
    }, error => {
      this._loaderService.hide();
    });
  }
  onDashboard() {
    this._router.navigate(['./dashboard/vendor-dashboard']);
  }
  onViewPictures() {
    const options: NgbModalOptions = {
      size: 'lg',
    };
    const modalRef = this._modal.open(ViewPicturesComponent, options);
    modalRef.componentInstance.orderId = this.orderId;
  }
  onViewComments() {
    const options: NgbModalOptions = {
      size: 'lg',
    };
    const modalRef = this._modal.open(ViewCommentComponent, options);
    modalRef.componentInstance.orderId = this.orderId;
  }
}
