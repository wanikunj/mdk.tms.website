import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PlaceNewOrderComponent } from './place-new-order/place-new-order.component';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PlaceVendorOrderComponent } from './place-vendor-order/place-vendor-order.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { UpdateVendorOrderComponent } from './update-vendor-order/update-vendor-order.component';
import { OrdersListComponent, OrderListCustomRendererComponent } from './orders-list/orders-list.component';
import { CarDetailsComponent } from './car-details/car-details.component';
import { AlwaysAuthGuard } from '../../shared/guard/auth.guard';
import { ViewVendorOrderComponent } from './view-vendor-order/view-vendor-order.component';
import { ViewPicturesComponent } from './view-pictures/view-pictures.component';
const routes: Routes = [{
  path: '',
  children: [
    { path: 'new-order', component: PlaceNewOrderComponent },
    { path: 'vendor-order', component: PlaceVendorOrderComponent },
    { path: 'order-list', component: OrdersListComponent },
    { path: 'edit-order/:id', component: UpdateVendorOrderComponent },
    { path: 'item-view/:id', component: CarDetailsComponent },
    { path: 'view-order/:id', component: ViewVendorOrderComponent }
  ], canActivate: [AlwaysAuthGuard]
}];

@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [PlaceNewOrderComponent, PlaceVendorOrderComponent, UpdateVendorOrderComponent, OrdersListComponent,
    OrderListCustomRendererComponent, ViewVendorOrderComponent,
    CarDetailsComponent,
    ViewPicturesComponent],
  entryComponents: [OrderListCustomRendererComponent, ViewPicturesComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class OrdersModule { }
