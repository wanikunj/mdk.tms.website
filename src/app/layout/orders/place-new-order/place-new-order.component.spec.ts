import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlaceNewOrderComponent } from './place-new-order.component';

describe('PlaceNewOrderComponent', () => {
  let component: PlaceNewOrderComponent;
  let fixture: ComponentFixture<PlaceNewOrderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlaceNewOrderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlaceNewOrderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
