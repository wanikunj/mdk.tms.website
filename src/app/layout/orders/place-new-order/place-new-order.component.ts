import { Component, OnInit } from '@angular/core';
import { OrderService } from '../../../shared/services/order.service';
import { Order } from '../../../model/order.model';

@Component({
  selector: 'app-place-new-order',
  templateUrl: './place-new-order.component.html',
  styleUrls: ['./place-new-order.component.scss']
})
export class PlaceNewOrderComponent implements OnInit {
  model: any;
  public order: Order;
  constructor(
    private _orderservice: OrderService,
  ) {
    this.order = new Order();
  }

  ngOnInit() {
  }

   addOrders() {
    this._orderservice.addOrders1(this.order)
      .subscribe(
        data => {
          if (data) {}

        }, error => {  });
  }
}
