import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'dashboard-vendor-orders',
  templateUrl: './vendor-orders.component.html',
  styleUrls: ['./vendor-orders.component.scss']
})
export class VendorOrdersComponent implements OnInit {

  defaultPagination: number;
  advancedPagination: number;
  paginationSize: number;
  disabledPagination: number;
  isDisabled: boolean;

  constructor() {
    this.defaultPagination = 1;
    this.advancedPagination = 1;
    this.paginationSize = 1;
    this.disabledPagination = 1;
    this.isDisabled = true;
  }

  ngOnInit() {
  }

  toggleDisabled() {
    this.isDisabled = !this.isDisabled;
  }
}
