import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BidOrdersComponent } from './bid-orders.component';

describe('BidOrdersComponent', () => {
  let component: BidOrdersComponent;
  let fixture: ComponentFixture<BidOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BidOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BidOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
