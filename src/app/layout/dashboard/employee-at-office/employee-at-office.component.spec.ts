import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeAtOfficeComponent } from './employee-at-office.component';

describe('EmployeeAtOfficeComponent', () => {
  let component: EmployeeAtOfficeComponent;
  let fixture: ComponentFixture<EmployeeAtOfficeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeAtOfficeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeAtOfficeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
