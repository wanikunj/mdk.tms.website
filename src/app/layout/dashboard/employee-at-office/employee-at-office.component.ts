import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-employee-at-office',
  templateUrl: './employee-at-office.component.html',
  styleUrls: ['./employee-at-office.component.scss'],
  animations: [routerTransition()]
})
export class EmployeeAtOfficeComponent implements OnInit {

  public alerts: Array<any> = [];
  public sliders: Array<any> = [];

  // bar chart
  public barChartOptions: any = {
      scaleShowVerticalLines: false,
      responsive: true
  };
  public barChartLabels: string[] = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'June',
      'July',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec'
  ];
  public barChartType: string = 'bar';
  public barChartLegend: boolean = true;

  public barChartData: any[] = [
      { data: [65, 59, 80, 81, 56, 55, 40,28,90,19,86,27], label: 'Orders' },
  ];

  // Doughnut
  public doughnutChartLabels: string[] = [
      'Placed',
      'On Hold',
      'Pending',
      'Delivered',
      'Receiving at Port'
  ];
  public doughnutChartData: number[] = [350, 450, 100,150,50];
  public doughnutChartType: string = 'doughnut';

  // events
  public chartClicked(e: any): void {
      // console.log(e);
  }

  public chartHovered(e: any): void {
      // console.log(e);
  }


  constructor() { }

  ngOnInit() {
  }
  
}
