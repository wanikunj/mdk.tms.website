import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-customer',
  templateUrl: './customer.component.html',
  styleUrls: ['./customer.component.scss'],
  animations: [routerTransition()]

})
export class CustomerComponent implements OnInit {


  public alerts: Array<any> = [];
  public sliders: Array<any> = [];

  // bar chart
  public barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels: string[] = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'June',
    'July',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];
  public barChartType = 'bar';
  public barChartLegend = true;

  public barChartData: any[] = [
    { data: [65, 59, 80, 81, 56, 55, 40, 28, 90, 19, 86, 27], label: 'Orders' },
  ];

  // Doughnut
  public doughnutChartLabels: string[] = [
    'Placed',
    'Delivered',
    'Pending'
  ];
  public doughnutChartData: number[] = [26, 14, 12];
  public doughnutChartType = 'doughnut';

  // events
  public chartClicked(e: any): void {
    // console.log(e);
  }

  public chartHovered(e: any): void {
    // console.log(e);
  }

  constructor() {

  }

  ngOnInit() { }
}
