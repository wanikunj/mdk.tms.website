import { Component, OnInit, OnDestroy } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { Router } from '@angular/router';
import { LoaderService } from '../../../shared/services/loader.service';
import { ToastrService } from 'ngx-toastr';
import { OrderService } from '../../../shared/services/order.service';
import { DriverFilterObj, OrderDetail } from '../../../model/driver.model';
import { CommonService } from '../../../shared/services/common.service';
import { OrderList } from '../../../model/order-grid.model';
import { PortOrderFilter } from '../../../model/port.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CommentComponent } from '../../bs-component/components/comment/comment.component';
import { Subscription } from 'rxjs';
import { UserLogin } from '../../../model/login-user.model';
import { GuardService } from '../../../shared/services/guard.service';

@Component({
  selector: 'app-employee-at-port',
  templateUrl: './employee-at-port.component.html',
  styleUrls: ['./employee-at-port.component.scss'],
  animations: [routerTransition()]
})
export class EmployeeAtPortComponent implements OnInit, OnDestroy {

  statusObj: OrderList;
  length;
  orderList: OrderDetail[];
  orderListCopy: OrderDetail[];
  orderCount: any = 0;
  filterObj: PortOrderFilter;
  orderStatusList = [
    { 'ID': '4', 'Value': 'Delivered', 'class': 'success' },
    { 'ID': '5', 'Value': 'Received at port', 'class': 'chocolate' }];
  bgClass: any = [];
  user$: Subscription;
  currentUser: UserLogin;
  username = '';
  portname = '';
  constructor(private router: Router, private _loaderService: LoaderService, private _guard: GuardService,
    private _toasterService: ToastrService, private _orderService: OrderService,
    private _commonService: CommonService, private _modalService: NgbModal) {
    this.filterObj = new PortOrderFilter();
    this.statusObj = new OrderList();
    this.orderList = [];
  }

  ngOnInit() {
    this.user$ = this._guard.user$.subscribe(user => {
      this.currentUser = user;
      if (this.currentUser.User != undefined || this.currentUser.User !== null) {
        this.username = this.currentUser.User.FirstName + ' ' + this.currentUser.User.LastName;
      }
    });
    this._loaderService.show();
    this.getOrderList();
    this.portname = this.currentUser.LocalPorts.LocalPortName;
  }

  ngOnDestroy() {
    this.user$.unsubscribe();
  }
  getOrderList(name?: any) {
    this.getPortOrders();
  }
  onDeliveredChecked(eve: any) {
    if (eve.target.checked === true) {
      this.filterObj.DeliveredToday = true;
    } else {
      this.filterObj.DeliveredToday = false;
    }
    this.getPortOrders();
  }
  getPortOrders() {
    this.bgClass = [];
    this._loaderService.show();
    this.filterObj.TimeOffset = this._commonService.getUserTimeZoneOffSet();
    this._orderService.getPortOrderList(this.filterObj).subscribe((data) => {
      if (data) {
        if (data.Orders != null) {
          this.orderListCopy = data.Orders;
          this.orderList = data.Orders;
          this.orderCount = data.TodayOrdersCount;
          this.orderList.forEach((value) => {
            this.orderStatusList.forEach((element) => {
              if (value.OrderStatusID == element.ID) {
                this.bgClass.push(element.class);
              }
            });
          });
        }
        this._loaderService.hide();
      } else {
        this._loaderService.hide();
      }
    }, error => {
      this._loaderService.hide();
    });
  }
  onStatusChange(statusId, index) {
    this._loaderService.show();
    this.statusObj.OrderStatusID = statusId;
    this.statusObj.TimeOffSet = this._commonService.getUserTimeZoneOffSet();
    this.statusObj.AuctionName = this.orderList[index].AuctionName;
    this.statusObj.IsActive = this.orderList[index].IsActive;
    this.statusObj.OrderId = this.orderList[index].OrderId;
    this.statusObj.VendorName = this.orderList[index].VendorName;
    this.statusObj.UpdateDate = this.orderList[index].UpdatedDate;
    this.statusObj.OrderDate = this.orderList[index].OrderDate;
    this.statusObj.VehicleName = this.orderList[index].VehicleTypeName;
    this.statusObj.UpdateBy = this.currentUser.User.UserId;
    this.statusObj.PaymentStatus = '';
    this.orderStatusList.filter((value) => {
      if (value.ID == statusId) {
        this.statusObj.OrderStatus = value.Value;
      }
    });
    this._orderService.UpdateOrderStatus(this.statusObj).subscribe((data) => {
      if (data) {
        this.getOrderList();
        this._loaderService.hide();
      } else {
        this._loaderService.hide();
      }
    }, error => {
      this._loaderService.hide();
    });
  }
  onFilter(value) {
    this._loaderService.show();
    if (value) {
      const filterargs = {
        'Chasis': value,
        'VehicleTypeName': value,
        'UpdatedDate': value,
        'DriverName': value
      };
      const filterKeys = Object.keys(filterargs);
      const val = [];
      let validFlag = false;
      this.orderListCopy.filter((item: any, index) => {
        validFlag = false;
        filterKeys.forEach(eachKey => {
          if (item[eachKey] != '' && item[eachKey] != undefined && item[eachKey] != null) {
            const itemValue = item[eachKey].toLowerCase();
            const filterValue = filterargs[eachKey].toLowerCase();
            if (itemValue.indexOf(filterValue) > -1) {
              validFlag = true;
            }
          }
        });
        if (validFlag) {
          val.push(item);
        }
      });
      if (val.length) {
        this.orderList = [...val];
        this._loaderService.hide();
      }
      this._loaderService.hide();
    } else {
      this.orderList = [...this.orderListCopy];
      this._loaderService.hide();
    }
  }
  onWriteComments(orderObj) {
    const modalRef = this._modalService.open(CommentComponent);
    modalRef.componentInstance.orderId = orderObj.OrderId;
    modalRef.componentInstance.userName = this.username;
    modalRef.componentInstance.userId = this.currentUser.User.UserId;
  }
  onTakePictures(orderObj) {
    this.router.navigate(['/take-picture/' + orderObj.OrderId]);
  }
  onDetail(id?: any) {
    this.router.navigate(['/neworders/item-view/' + id]);
  }
}
