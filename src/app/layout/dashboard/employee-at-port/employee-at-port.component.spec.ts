import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeAtPortComponent } from './employee-at-port.component';

describe('EmployeeAtPortComponent', () => {
  let component: EmployeeAtPortComponent;
  let fixture: ComponentFixture<EmployeeAtPortComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeAtPortComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeAtPortComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
