import { Component, OnInit, OnDestroy } from '@angular/core';
import { Auction, HelperClass } from '../../../model/vendor-order.model';
import { OrderList } from '../../../model/order-grid.model';
import { LocalDataSource, ViewCell } from 'ng2-smart-table';
import { LoaderService } from '../../../shared/services/loader.service';
import { ToastrService } from 'ngx-toastr';
import { OrderService } from '../../../shared/services/order.service';
import { routerTransition } from '../../../router.animations';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { UserLogin } from '../../../model/login-user.model';
import { GuardService } from '../../../shared/services/guard.service';
import { DriverFilterObj } from '../../../model/driver.model';
import { FilterObj } from '../../../model/order.model';

@Component({
  selector: 'app-vendor-dashboard',
  templateUrl: './vendor-dashboard.component.html',
  styleUrls: ['./vendor-dashboard.component.scss'],
  animations: [routerTransition()]
})
export class VendorDashboardComponent implements OnInit, OnDestroy {

  auctionList: Auction[];
  vendorList: HelperClass[];
  orderList: OrderList[];
  orderListCopy: OrderList[];
  AuctionID = '';
  orderDate = '';
  user$: Subscription;
  currentUser: UserLogin;
  source = new LocalDataSource();
  filter: FilterObj;
  settings = {
    actions: false,
    columns: {
      AuctionName: {
        title: 'Auction Name',
        filter: false
      },
      OrderDate: {
        title: 'Order Date',
        filter: false,
      },
      VehicleName: {
        title: 'Vehicle Name',
        filter: false,
        type: 'custom',
        valuePrepareFunction: (cell, orderList) => {
          return { 'value': orderList.VehicleName, 'title': 'View Order' };
        },
        renderComponent: VendorDashboardRendererComponent
      },
      OrderStatusID: {
        title: 'Order Status',
        filter: false,
        type: 'custom',
        valuePrepareFunction: (cell, orderList) => {
          return { 'value': orderList.OrderStatusID, 'title': 'Order Status' };
        },
        renderComponent: VendorDashboardRendererComponent,
      },
      UpdateDate: {
        title: 'Order Status Date',
        filter: false,
        valuePrepareFunction: (cell, orderList) => {
          const val = orderList.UpdateDate.split(' ');
          return val[0] + '  at  ' + val[1] + ' ' + val[2];
        }
      },
      PaymentStatus: {
        title: 'Payment Status',
        filter: false,
      }
    },
    pager: {
      display: true,
      perPage: 20,
    },
    hideSubHeader: true,
    sort: true,
  };
  pendingOrdersCount = 0;
  pendingCarCount = 0;
  pendingTruckCount = 0;
  pendingSpecialCount = 0;
  pendingInvoices = 0;
  constructor(private _loaderService: LoaderService,
    private _toasterService: ToastrService, private _guard: GuardService,
    private _orderService: OrderService) {
    this.auctionList = [];
    this.vendorList = [];
    this.filter = new FilterObj();
  }

  ngOnInit() {
    this.user$ = this._guard.user$.subscribe(user => {
      this.currentUser = user;
      if (this.currentUser.Vendors != null) {
        this.filter.VendorID = this.currentUser.Vendors.VendorID;
      }
    });
    this.orderList = [];
    this.orderListCopy = [];
    this.getVendorPendingItems();
    this.getVendorOrderlist();
    this.getAuctionList();
  }
  ngOnDestroy() {
    this.user$.unsubscribe();
  }
  getAuctionList(): any {
    this._loaderService.show();
    this._orderService.getAuctionList().subscribe(
      data => {
        this._loaderService.hide();
        if (data) {
          this.auctionList = data;
        }
      }, error => {
        this._loaderService.hide();
      });
  }

  getVendorOrderlist() {
    this._loaderService.show();
    this._orderService.getVendorOrderList(this.filter).subscribe(
      data => {
        this._loaderService.hide();
        if (data) {
          this.orderList = data;
          this.source = new LocalDataSource(this.orderList);
        }
      }, error => {
        this._loaderService.hide();
      });
  }
  getVendorPendingItems() {
    this.pendingInvoices = 0;
    this.pendingOrdersCount = 0;
    this.pendingCarCount = 0;
    this.pendingTruckCount = 0;
    this.pendingSpecialCount = 0;
    this._orderService.getVendorPendingItems(this.currentUser.Vendors.VendorID).subscribe(
      data => {
        this._loaderService.hide();
        if (data) {
          this.pendingInvoices = data.PendingInvoices;
          this.pendingOrdersCount = data.PendingOrders;
          this.pendingCarCount = data.PendingCars;
          this.pendingTruckCount = data.PendingTrucks;
          this.pendingSpecialCount = data.PendingSpecial;
        }
      }, error => {
        this._loaderService.hide();
      });
  }

  onSearch(query: string = '') {
    if (query) {
      this.source.setFilter([
        {
          field: 'OrderId',
          search: query
        },
        {
          field: 'OrderStatus',
          search: query
        },
        {
          field: 'AuctionName',
          search: query
        },
        {
          field: 'OrderDate',
          search: query
        },
        {
          field: 'UpdateDate',
          search: query
        },
        {
          field: 'VehicleName',
          search: query
        },
        {
          field: 'PaymentStatus',
          search: query
        }
      ], false, true);
    } else {
      this.source = new LocalDataSource(this.orderList);
    }
  }

  onFilter(date?: any) {
    let orderDate = this.orderDate;
    if (date) {
      orderDate = this.orderDate['month']
        + '-' + this.orderDate['day']
        + '-' + this.orderDate['year'];
    }
    this.filter.OrderDate = orderDate;
    this.filter.AuctionID = this.AuctionID;
    this._loaderService.show();
    this._orderService.getVendorOrderList(this.filter).subscribe((data) => {
      this._loaderService.hide();
      this.source = new LocalDataSource(data);
    }, error => {
      this._loaderService.hide();
    });
  }

  onResetFilter() {
    this.AuctionID = '';
    this.orderDate = '';
    this.filter.AuctionID = '';
    this.filter.OrderDate = '';
    this.getVendorOrderlist();
  }

}

@Component({
  template: `
  <a *ngIf="this.value.title === 'Order Status'" class="btn label text-white bg-{{bgClass}}" (click)="navigateToSomeRoute()">{{title}}</a>
  <a *ngIf="this.value.title !== 'Order Status'" class="btn btn-link font-text" (click)="navigateToSomeRoute()">{{title}}</a>
  `,
  styleUrls: ['./vendor-dashboard.component.scss'],
})
export class VendorDashboardRendererComponent implements ViewCell, OnInit {
  value: any;
  rowData: any;
  title: any;
  bgClass = '';
  orderStatusList = [{ 'ID': '1', 'Value': 'Pending', 'class': 'info' },
  { 'ID': '2', 'Value': 'Created', 'class': 'primary' },
  { 'ID': '3', 'Value': 'Picked', 'class': 'warning' },
  { 'ID': '4', 'Value': 'Delivered', 'class': 'success' },
  { 'ID': '5', 'Value': 'Received at port', 'class': 'chocolate' }];
  constructor(private router: Router) {
  }

  renderValue: string;


  ngOnInit() {
    if (this.value.title === 'Order Status') {
      this.title = this.rowData.OrderStatus;
      this.orderStatusList.filter((ele) => {
        if (this.value.value == ele.ID) {
          this.bgClass = ele.class;
        }
      });
    } else if (this.value.title === 'View Order') {
      this.title = this.value.value;
    }
  }
  navigateToSomeRoute() {
    if (this.value.title === 'View Order') {
      const id = this.rowData.OrderId;
      console.log('id', id);
      this.router.navigate(['/neworders/view-order/' + id]);
    }
  }
}
