import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { OrderList } from '../../../model/order-grid.model';
import { LoadChildren, Router } from '@angular/router';
import { LoaderService } from '../../../shared/services/loader.service';
import { OrderService } from '../../../shared/services/order.service';
import { LocalDataSource, ViewCell } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ChangeStatusComponent } from '../../bs-component/components/change-status/change-status.component';
import { Auction, HelperClass } from '../../../model/vendor-order.model';
import { Vendor } from '../../../model/vendor.model';
import { ToastrService } from 'ngx-toastr';
import { element } from 'protractor';
import { Subscription } from 'rxjs';
import { GuardService } from '../../../shared/services/guard.service';
import { UserLogin } from '../../../model/login-user.model';
import { CommonService } from '../../../shared/services/common.service';


@Component({
  selector: 'app-manager-dashboard',
  templateUrl: './manager-dashboard.component.html',
  styleUrls: ['./manager-dashboard.component.scss'],
  animations: [routerTransition()]
})
export class ManagerDashboardComponent implements OnInit {

  auctionList: Auction[];
  vendorList: HelperClass[];
  orderList: OrderList[];
  orderListCopy: OrderList[];
  VendorID = '';
  AuctionID = '';
  orderDate = '';
  source = new LocalDataSource();
  pendingOrdersCount = 0;
  pendingCarCount = 0;
  pendingTruckCount = 0;
  pendingSpecialCount = 0;
  pendingInvoices = 0;
  settings = {
    actions: false,
    columns: {
      VendorName: {
        title: 'Vendor Name',
        filter: false
      },
      AuctionName: {
        title: 'Auction Name',
        filter: false
      },
      OrderDate: {
        title: 'Order Date',
        filter: false,
      },
      VehicleName: {
        title: 'Vehicle Name',
        filter: false,
        type: 'custom',
        valuePrepareFunction: (cell, orderList) => {
          return { 'value': orderList.VehicleName, 'title': 'Update Order' };
        },
        renderComponent: ManagerDashboardRendererComponent
      },
      OrderStatusID: {
        title: 'Order Status',
        filter: false,
        type: 'custom',
        valuePrepareFunction: (cell, orderList) => {
          return { 'value': orderList.OrderStatusID, 'title': 'Order Status' };
        },
        renderComponent: ManagerDashboardRendererComponent,
        onComponentInitFunction: (instance) => {
          instance.save.subscribe(row => {
            if (row) {
              this.getOrderlist();
            }
          });
        }
      },
      UpdateDate: {
        title: 'Order Status Date',
        filter: false,
        valuePrepareFunction: (cell, orderList) => {
          const val = orderList.UpdateDate.split(' ');
          return val[0] + '  at  ' + val[1] + ' ' + val[2];
        }
      },
      PaymentStatus: {
        title: 'Payment Status',
        filter: false,
        // valuePrepareFunction: (cell, orderList) => {
        //   return 'Pending';
        // },
      }
    },
    pager: {
      display: true,
      perPage: 20,
    },
    hideSubHeader: true,
    sort: true,
  };
  constructor(private _loaderService: LoaderService,
    private _toasterService: ToastrService,
    private _orderService: OrderService) {
    this.auctionList = [];
    this.vendorList = [];
  }

  ngOnInit() {
    this.orderList = [];
    this.orderListCopy = [];
    this.getManagerPendingItems();
    this.getOrderlist();
    this.getAuctionList();
    this.getVendorList();
  }
  getVendorList(): any {
    this._loaderService.show();
    this._orderService.getVendorName().subscribe(
      data => {
        this._loaderService.hide();
        if (data) {
          this.vendorList = data;
        }
      }, error => {
        this._loaderService.hide();
      });
  }
  getAuctionList(): any {
    this._loaderService.show();
    this._orderService.getAuctionList().subscribe(
      data => {
        this._loaderService.hide();
        if (data) {
          this.auctionList = data;
        }
      }, error => {
        this._loaderService.hide();
      });
  }

  getOrderlist() {
    this.pendingOrdersCount = 0;
    this.pendingCarCount = 0;
    this.pendingTruckCount = 0;
    this.pendingSpecialCount = 0;
    this._loaderService.show();
    this._orderService.getAllOrder().subscribe(
      data => {
        this._loaderService.hide();
        if (data) {
          this.orderList = data;
          this.orderList.forEach(ele => {
            if (ele.OrderStatusID == 1) {
              this.pendingOrdersCount = this.pendingOrdersCount + 1;
              if (ele.VehicleTypeID == 1) {
                this.pendingCarCount = this.pendingCarCount + 1;
              } else if (ele.VehicleTypeID == 2) {
                this.pendingTruckCount = this.pendingTruckCount + 1;
              } else if (ele.VehicleTypeID == 3) {
                this.pendingSpecialCount = this.pendingSpecialCount + 1;
              }
            }
          });
          this.source = new LocalDataSource(this.orderList);
        }
      }, error => {
        this._loaderService.hide();
      });
  }

  getManagerPendingItems() {
    this.pendingInvoices = 0;
    this._orderService.getManagerPendingItems().subscribe(
      data => {
        this._loaderService.hide();
        if (data) {
          this.pendingInvoices = data.PendingInvoices;
          // this.pendingOrdersCount = data.PendingOrders;
          // this.pendingCarCount = data.PendingCars;
          // this.pendingTruckCount = data.PendingTrucks;
          // this.pendingSpecialCount = data.PendingSpecial;
        }
      }, error => {
        this._loaderService.hide();
      });
  }

  onSearch(query: string = '') {
    if (query) {
      this.source.setFilter([
        {
          field: 'OrderId',
          search: query
        },
        {
          field: 'VendorName',
          search: query
        },
        {
          field: 'OrderStatus',
          search: query
        },
        {
          field: 'VehicleName',
          search: query
        },
        {
          field: 'AuctionName',
          search: query
        }, {
          field: 'OrderDate',
          search: query
        },
        {
          field: 'UpdateDate',
          search: query
        },
        {
          field: 'PaymentStatus',
          search: query
        },
      ], false, true);
    } else {
      this.source = new LocalDataSource(this.orderList);
    }
  }

  onFilter(date?: any) {
    let orderDate = this.orderDate;
    if (date) {
      orderDate = this.orderDate['month']
        + '-' + this.orderDate['day']
        + '-' + this.orderDate['year'];
    }
    const filter = {
      'VendorID': this.VendorID,
      'AuctionID': this.AuctionID,
      'OrderDate': orderDate
    };
    console.log('filter', filter);
    this._loaderService.show();
    this._orderService.SearchManagerOrders(filter).subscribe((data) => {
      this._loaderService.hide();
      this.source = new LocalDataSource(data);
    }, error => {
      this._loaderService.hide();
    });
  }

  onResetFilter() {
    this.VendorID = '';
    this.AuctionID = '';
    this.orderDate = '';
    this.getOrderlist();
  }
}
@Component({
  template: `
  <a *ngIf="this.value.title === 'Order Status'" class="btn label text-white bg-{{bgClass}}" (click)="navigateToSomeRoute()">{{title}}</a>
  <a *ngIf="this.value.title !== 'Order Status'" class="btn btn-link font-text" (click)="navigateToSomeRoute()">{{title}}</a>
  `,
  styleUrls: ['./manager-dashboard.component.scss'],
})
export class ManagerDashboardRendererComponent implements ViewCell, OnInit {
  value: any;
  rowData: any;
  title: any;
  bgClass = '';
  user$: Subscription;
  currentUser: UserLogin;
  userId: any;
  orderStatusList = [{ 'ID': '1', 'Value': 'Pending', 'class': 'info' },
  { 'ID': '2', 'Value': 'Created', 'class': 'primary' },
  { 'ID': '3', 'Value': 'Picked', 'class': 'warning' },
  { 'ID': '4', 'Value': 'Delivered', 'class': 'success' },
  { 'ID': '5', 'Value': 'Received at port', 'class': 'chocolate' }];
  @Output() save: EventEmitter<any> = new EventEmitter();
  constructor(private router: Router, private _modalService: NgbModal,
    private _toasterService: ToastrService, private _guard: GuardService,
    private _commonService: CommonService,
    private _loaderService: LoaderService, private _orderService: OrderService) {
  }

  renderValue: string;


  ngOnInit() {
    this.user$ = this._guard.user$.subscribe(user => {
      this.currentUser = user;
      if (this.currentUser.User != undefined || this.currentUser.User !== null) {
        this.userId = this.currentUser.User.UserId;
      }
    });
    if (this.value.title === 'Payment Status') {
      this.title = this.value.value;
    } else if (this.value.title === 'Order Status') {
      this.title = this.rowData.OrderStatus;
      this.orderStatusList.filter((ele) => {
        if (this.value.value == ele.ID) {
          this.bgClass = ele.class;
        }
      });
    } else if (this.value.title === 'Update Order') {
      this.title = this.value.value;
    }
  }
  navigateToSomeRoute() {
    if (this.value.title === 'Payment Status') {
      const modalRef = this._modalService.open(ChangeStatusComponent);
      modalRef.componentInstance.titleFrom = this.value.title;
      modalRef.componentInstance.statusValue = this.value.value;
      modalRef.result.then((result) => {
        if (result) {
          console.log('value updated');
        }
      });
    } else if (this.value.title === 'Order Status') {
      const modalRef = this._modalService.open(ChangeStatusComponent);
      modalRef.componentInstance.titleFrom = this.value.title;
      // modalRef.componentInstance.statusValue = this.value.value;
      modalRef.componentInstance.statusValue = this.value.value;
      modalRef.result.then((result) => {
        if (result) {
          if (result.ID !== this.value.value) {
            this.changeOrderStatus(result);
          }
          // if (result !== this.rowData.OrderStatusId) {
          //   this.changeOrderStatus();
          // }
        }
      });
    } else if (this.value.title === 'Update Order') {
      const id = this.rowData.OrderId;
      console.log('id', id);
      this.router.navigate(['/neworders/edit-order/' + id]);
    }
  }

  changeOrderStatus(orderStatusId) {
    this._loaderService.show();
    const statusObj = this.rowData;
    statusObj.OrderStatusID = orderStatusId.ID;
    statusObj.OrderStatus = orderStatusId.Value;
    statusObj.UpdateBy = this.currentUser.User.UserId;
    statusObj.TimeOffset = this._commonService.getUserTimeZoneOffSet();
    this._orderService.UpdateOrderStatus(statusObj).subscribe((data) => {
      this._loaderService.hide();
      if (data === true) {
        this._toasterService.success('Order Updated Successfully!!');
        this.save.emit('true');
      } else {
        this._toasterService.error('Order status not Updated !!');
      }
    }, error => {
      this._loaderService.hide();
      this._toasterService.error('Server Error occured !!');
    });
  }
}
