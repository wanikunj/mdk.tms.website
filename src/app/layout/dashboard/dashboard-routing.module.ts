import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { TransportationWorkerComponent } from './transportation-worker/transportation-worker.component';
import { CustomerComponent } from './customer/customer.component';
import { EmployeeAtDestinationComponent } from './employee-at-destination/employee-at-destination.component';
import { EmployeeAtPortComponent } from './employee-at-port/employee-at-port.component';
import { EmployeeAtOfficeComponent } from './employee-at-office/employee-at-office.component';
import { ManagerDashboardComponent } from './manager-dashboard/manager-dashboard.component';
import { DriverDashboardComponent } from './driver-dashboard/driver-dashboard.component';
import { AlwaysAuthGuard } from '../../shared/guard/auth.guard';
import { LocalPortGuard } from '../../shared/guard/local-port.guard';
import { ManagerGuard } from '../../shared/guard/manager.guard';
import { DriverGuard } from '../../shared/guard/driver.guard';
import { AdminGuard } from '../../shared/guard/admin.guard';
import { TakePicturesLinkComponent } from './take-pictures-link/take-pictures-link.component';
import { VendorDashboardComponent } from './vendor-dashboard/vendor-dashboard.component';
import { VendorGuard } from '../../shared/guard/vendor.guard';

const routes: Routes = [{
    path: '',
    children: [
        {
            path: '', component: DashboardComponent,
            canActivate: [AlwaysAuthGuard]
        },
        // {
        //     path: 'transportation-worker',
        //     component: TransportationWorkerComponent
        // },
        // {
        //     path: 'customer',
        //     component: CustomerComponent
        // },
        // {
        //     path: 'office-employee',
        //     component: EmployeeAtOfficeComponent
        // },
        // {
        //     path: 'destination-employee',
        //     component: EmployeeAtDestinationComponent
        // },
        {
            path: 'port-employee',
            component: EmployeeAtPortComponent,
            canActivate: [AlwaysAuthGuard, LocalPortGuard]
        },
        {
            path: 'manager-dashboard',
            component: ManagerDashboardComponent,
            canActivate: [AlwaysAuthGuard, ManagerGuard, AdminGuard]
        },
        {
            path: 'driver-dashboard',
            component: DriverDashboardComponent,
            canActivate: [AlwaysAuthGuard, DriverGuard]
        },
        {
            path: 'vendor-dashboard',
            component: VendorDashboardComponent,
            canActivate: [AlwaysAuthGuard, VendorGuard]
        },
        {
            path: 'take-picture/:id',
            component: TakePicturesLinkComponent,
            canActivate: [AlwaysAuthGuard]
        },
    ],
    runGuardsAndResolvers: 'always',
}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DashboardRoutingModule {
}
