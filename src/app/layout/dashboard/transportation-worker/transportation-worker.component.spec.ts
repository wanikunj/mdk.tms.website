import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TransportationWorkerComponent } from './transportation-worker.component';

describe('TransportationWorkerComponent', () => {
  let component: TransportationWorkerComponent;
  let fixture: ComponentFixture<TransportationWorkerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TransportationWorkerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TransportationWorkerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
