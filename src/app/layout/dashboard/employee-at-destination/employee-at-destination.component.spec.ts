import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmployeeAtDestinationComponent } from './employee-at-destination.component';

describe('EmployeeAtDestinationComponent', () => {
  let component: EmployeeAtDestinationComponent;
  let fixture: ComponentFixture<EmployeeAtDestinationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmployeeAtDestinationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmployeeAtDestinationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
