import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-employee-at-destination',
  templateUrl: './employee-at-destination.component.html',
  styleUrls: ['./employee-at-destination.component.scss'],
  animations: [routerTransition()]
})
export class EmployeeAtDestinationComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}