import { Component, OnInit, OnDestroy } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from '../../../shared/services/loader.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PictureModel, VehicleModel } from '../../../model/picture.model';
import { PictureService } from '../../../shared/services/picture.service';
import { CommonService } from '../../../shared/services/common.service';
import { GuardService } from '../../../shared/services/guard.service';
import { Subscription, VirtualTimeScheduler } from 'rxjs';
import { UserModel, UserLogin } from '../../../model/login-user.model';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-take-pictures-link',
  templateUrl: './take-pictures-link.component.html',
  styleUrls: ['./take-pictures-link.component.scss']
})
export class TakePicturesLinkComponent implements OnInit, OnDestroy {

  orderId: any;
  imageList: PictureModel[] = [];
  vehicleObj: VehicleModel;
  pictureObj: PictureModel;
  user$: Subscription;
  currentUser: UserLogin;
  apiUrl: string;
  deviceInfo = null;
  isMobileFlag = false;
  class: any;
  color = ['primary', 'warning', 'success', 'info'];
  bgClass = [];
  public screenOrientation: any;
  constructor(private _toasterService: ToastrService, private _guard: GuardService,
    private _loaderService: LoaderService, private _commonService: CommonService,
    private router: Router, private _pictureServie: PictureService,
    private route: ActivatedRoute) {
    this.vehicleObj = new VehicleModel();
    this.apiUrl = environment.imagePath;
  }

  ngOnInit() {
    this.pictureObj = new PictureModel();
    this.orderId = this.route.snapshot.paramMap.get('id');
    this.getPictureList();
    this.user$ = this._guard.user$.subscribe(user => {
      this.currentUser = user;
      if (this.currentUser) {
        if (this.currentUser.User != null) {
          this.pictureObj.UserName = this.currentUser.User.FirstName + ' ' + this.currentUser.User.LastName;
        }
      }
    });
    this.pictureObj.OrderID = this.orderId;
  }
  ngOnDestroy() {
    this.user$.unsubscribe();
  }
  getPictureList() {
    this._loaderService.show();
    this._pictureServie.getPictureList(this.orderId).subscribe((data) => {
      if (data) {
        this.imageList = data.Pictures;
        let i = 0;
        this.imageList.forEach((ele, index) => {
          if (i < 4) {
            this.bgClass.push(this.color[i]);
            i = i + 1;
          } else {
            i = 0;
            this.bgClass.push(this.color[i]);
            i = i + 1;
          }
        });
        this.vehicleObj = data.Vehicle;
      }
      this._loaderService.hide();
    }, error => {
      this._loaderService.hide();
    });
  }
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this._loaderService.show();
      const file = event.target.files[0];
      const fileReader = new FileReader();
      fileReader.onloadend = (e) => {
        const rawData = fileReader.result;
        this._loaderService.hide();
        this.pictureObj.PicturePath = rawData.toString();
        this.addPicture();
      };
      fileReader.readAsDataURL(file);
    }
  }
  addPicture() {
    this._loaderService.show();
    this.pictureObj.TimeOffset = this._commonService.getUserTimeZoneOffSet();
    this.pictureObj.CreatedBy = this.currentUser.User.UserId;
    this._pictureServie.addPicture(this.pictureObj).subscribe(data => {
      if (data) {
        this._loaderService.hide();
        const id = data.PictureID;
        if (id > 0) {
          this.imageList.unshift(data);
        }
      }
      this._loaderService.hide();
    }, error => {
      this._loaderService.hide();
    });
  }
  updateComments(pictureObj: PictureModel) {
    this._loaderService.show();
    pictureObj.TimeOffset = this._commonService.getUserTimeZoneOffSet();
    pictureObj.UserName = this.currentUser.User.FirstName + ' ' + this.currentUser.User.LastName;
    this._pictureServie.updatePictureComment(pictureObj).subscribe((data) => {
      if (data) {
        this._toasterService.success('Comment Added Successfully!');
        this._loaderService.hide();
      } else {
        this._toasterService.error('Error at adding Comment!');
        this._loaderService.hide();
      }
    }, error => {
      this._toasterService.error('Server Error Occurred!');
      this._loaderService.hide();
    });
  }
}
