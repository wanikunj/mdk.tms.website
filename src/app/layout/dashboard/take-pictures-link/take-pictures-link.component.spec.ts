import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TakePicturesLinkComponent } from './take-pictures-link.component';

describe('TakePicturesLinkComponent', () => {
  let component: TakePicturesLinkComponent;
  let fixture: ComponentFixture<TakePicturesLinkComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TakePicturesLinkComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TakePicturesLinkComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
