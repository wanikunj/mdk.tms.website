import { Component, OnInit, OnDestroy } from '@angular/core';
import { routerTransition } from '../../router.animations';
import { Subscription } from 'rxjs';
import { GuardService } from '../../shared/services/guard.service';
import { UserLogin } from '../../model/login-user.model';
import { Router } from '@angular/router';
import { AuthService } from '../../shared/services/auth.service';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.scss'],
    animations: [routerTransition()]
})
export class DashboardComponent implements OnInit, OnDestroy {
    public alerts: Array<any> = [];
    public sliders: Array<any> = [];
    user$: Subscription;
    currentUser: UserLogin;
    constructor(private _guard: GuardService, private router: Router, private _auth: AuthService) {
    }

    ngOnInit() {
        this.user$ = this._guard.user$.subscribe(user => {
            this.currentUser = user;
            if (this.currentUser.User !== null) {
                if (this.currentUser.User.UserType === 'Admin' || this.currentUser.User.UserType === 'Manager') {
                    this.router.navigate(['/dashboard/manager-dashboard']);
                } else if (this.currentUser.User.UserType === 'Driver') {
                    this.router.navigate(['/dashboard/driver-dashboard']);
                } else if (this.currentUser.User.UserType === 'Local Port Employee') {
                    this.router.navigate(['/dashboard/port-employee']);
                } else if (this.currentUser.User.UserType === 'Vendor') {
                    this.router.navigate(['/dashboard/vendor-dashboard']);
                }
            } else {
                this._guard.logoutCurrentUser();
              //  this._auth.removeUser();
                this.router.navigate(['/login']);
            }
        });
    }

    ngOnDestroy() {
        this.user$.unsubscribe();
    }
}
