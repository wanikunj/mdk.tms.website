import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbCarouselModule, NgbAlertModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ChartsModule as Ng2Charts } from 'ng2-charts';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { StatModule } from '../../shared';
import { BidOrdersComponent } from './components/bid-orders/bid-orders.component';
import { VendorOrdersComponent } from './components/vendor-orders/vendor-orders.component';
import { TransportationWorkerComponent } from './transportation-worker/transportation-worker.component';
import { CustomerComponent } from './customer/customer.component';
import { EmployeeAtPortComponent } from './employee-at-port/employee-at-port.component';
import { EmployeeAtDestinationComponent } from './employee-at-destination/employee-at-destination.component';
import { EmployeeAtOfficeComponent } from './employee-at-office/employee-at-office.component';
import { ManagerDashboardComponent, ManagerDashboardRendererComponent } from './manager-dashboard/manager-dashboard.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { DriverDashboardComponent } from './driver-dashboard/driver-dashboard.component';
import { TakePicturesLinkComponent } from './take-pictures-link/take-pictures-link.component';
import { HttpClient } from '@angular/common/http';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { VendorDashboardComponent, VendorDashboardRendererComponent } from './vendor-dashboard/vendor-dashboard.component';


// AoT requires an exported function for factories
export const createTranslateLoader = (http: HttpClient) => {
    /* for development
    return new TranslateHttpLoader(
        http,
        '/start-angular/SB-Admin-BS4-Angular-6/master/dist/assets/i18n/',
        '.json'
    ); */
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
  };

@NgModule({
    imports: [
        CommonModule,
        NgbCarouselModule.forRoot(),
        NgbAlertModule.forRoot(),
        DashboardRoutingModule,
        StatModule,
        SharedModule,
      //  TranslateModule.forRoot(),
        NgbModule.forRoot(),
        Ng2Charts,
    ],
    declarations: [
        DashboardComponent,
        BidOrdersComponent,
        VendorOrdersComponent,
        TransportationWorkerComponent,
        CustomerComponent,
        EmployeeAtPortComponent,
        EmployeeAtDestinationComponent,
        EmployeeAtOfficeComponent,
        ManagerDashboardComponent,
        ManagerDashboardRendererComponent,
        DriverDashboardComponent,
        TakePicturesLinkComponent,
        VendorDashboardComponent,
        VendorDashboardRendererComponent
    ],
    entryComponents: [ManagerDashboardRendererComponent, VendorDashboardRendererComponent]
})
export class DashboardModule { }
