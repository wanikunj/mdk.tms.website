import { Component, OnInit, DebugElement, OnDestroy } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { Router } from '@angular/router';
import { LoaderService } from '../../../shared/services/loader.service';
import { ToastrService } from 'ngx-toastr';
import { OrdersListComponent } from '../../orders/orders-list/orders-list.component';
import { OrderService } from '../../../shared/services/order.service';
import { OrderList } from '../../../model/order-grid.model';
import { DriverFilterObj, OrderDetail } from '../../../model/driver.model';
import { CommonService } from '../../../shared/services/common.service';
import { Order } from '../../../model/order.model';
import { CommentComponent } from '../../bs-component/components/comment/comment.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs';
import { UserLogin } from '../../../model/login-user.model';
import { GuardService } from '../../../shared/services/guard.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-driver-dashboard',
  templateUrl: './driver-dashboard.component.html',
  styleUrls: ['./driver-dashboard.component.scss'],
  animations: [routerTransition()]
})
export class DriverDashboardComponent implements OnInit, OnDestroy {

  statusObj: OrderList;
  length;
  orderList: OrderDetail[];
  orderListCopy: OrderDetail[];
  orderCount: any = 0;
  filterObj: DriverFilterObj;
  orderStatus = '2';
  todayDate = new Date();
  date;
  filterList = [{ 'ID': '2', 'Value': 'Ready' },
  { 'ID': '3', 'Value': 'Picked' },
  { 'ID': '4', 'Value': 'All' }];
  orderStatusList = [
    { 'ID': '2', 'Value': 'Ready', 'class': 'primary' },
    { 'ID': '3', 'Value': 'Picked', 'class': 'warning' },
    { 'ID': '4', 'Value': 'Delivered', 'class': 'success' }];
  bgClaas = [];
  filterargs: OrderDetail;
  user$: Subscription;
  currentUser: UserLogin;
  username = '';
  constructor(private router: Router, private _loaderService: LoaderService, private _guard: GuardService,
    private _toasterService: ToastrService, private _orderService: OrderService, private translate: TranslateService,
    private _commonService: CommonService, private _modalService: NgbModal) {
    this.filterObj = new DriverFilterObj();
    this.statusObj = new OrderList();
    this.orderList = [];
  }

  ngOnInit() {
    this.user$ = this._guard.user$.subscribe(user => {
      this.currentUser = user;
      if (this.currentUser.User != undefined || this.currentUser.User !== null) {
        this.username = this.currentUser.User.FirstName + ' ' + this.currentUser.User.LastName;
      }
    });
    this.date = this.todayDate.getFullYear() + '/' + (this.todayDate.getMonth() + 1) + '/' + this.todayDate.getDate();
    this._loaderService.show();
    this.getOrderList();
  }


  ngOnDestroy() {
    this.user$.unsubscribe();
  }
  getOrderList(name?: any) {
    this.bgClaas = [];
    this._loaderService.show();
    if (name) {
      this.filterObj.FilterName = name;
    }
    this.filterObj.TimeOffSet = this._commonService.getUserTimeZoneOffSet();
    console.log('obj', this.filterObj);
    this._orderService.getDriverOrderList(this.filterObj).subscribe((data) => {
      if (data) {
        this.orderListCopy = data.Orders;
        this.orderList = data.Orders;
        this.length = data.Orders.length;
        this.orderCount = data.TodayOrdersCount;
        // this.translate.get(
        //   ['DRIVER.DASHBOARD.MESSAGE'],
        //   { username: this.username, orderCount: this.orderCount , date: this.date}
        // )
        //   .subscribe(val => {
        //     this.username = val['DRIVER.DASHBOARD.MESSAGE'];
        //   });
        this.orderList.forEach((value) => {
          this.orderStatusList.forEach((element) => {
            if (value.OrderStatusID == element.ID) {
              this.bgClaas.push(element.class);
            }
          });
        });
        console.log('obj', this.orderList);
        this._loaderService.hide();
      } else {
        this._loaderService.hide();
      }
    }, error => {
      this._loaderService.hide();
    });
  }
  onDetail(id?: any) {

    this.router.navigate(['/neworders/item-view/' + id]);
  }
  onStatusChange(statusId, index) {
    this._loaderService.show();
    this.statusObj.OrderStatusID = statusId;
    this.statusObj.TimeOffSet = this._commonService.getUserTimeZoneOffSet();
    this.statusObj.AuctionName = this.orderList[index].AuctionName;
    this.statusObj.IsActive = this.orderList[index].IsActive;
    this.statusObj.OrderId = this.orderList[index].OrderId;
    this.statusObj.VendorName = this.orderList[index].VendorName;
    this.statusObj.UpdateDate = this.orderList[index].UpdatedDate;
    this.statusObj.OrderDate = this.orderList[index].OrderDate;
    this.statusObj.VehicleName = this.orderList[index].VehicleTypeName;
    this.statusObj.UpdateBy = this.currentUser.User.UserId;
    this.statusObj.PaymentStatus = '';
    this.orderStatusList.filter((value) => {
      if (value.ID == statusId) {
        this.statusObj.OrderStatus = value.Value;
      }
    });
    console.log('onbj', this.statusObj);
    this._orderService.UpdateOrderStatus(this.statusObj).subscribe((data) => {
      if (data) {
        this.getOrderList();
        this._loaderService.hide();
      } else {
        this._loaderService.hide();
      }
    }, error => {
      this._loaderService.hide();
    });
  }
  onFilter(value) {
    this._loaderService.show();
    if (value) {
      const filterargs = {
        'AuctionName': value,
        'OrderDate': value,
        'Lot': value,
        'CarName': value,
        'Chasis': value,
        'PickupLoc': value,
        'DestLoc': value,
        'VehicleTypeName': value,
        'UpdatedDate': value,
        'VendorName': value
      };
      const filterKeys = Object.keys(filterargs);
      const val = [];
      let validFlag = false;
      this.orderListCopy.filter((item: any, index) => {
        validFlag = false;
        filterKeys.forEach(eachKey => {
          if (item[eachKey] != '' && item[eachKey] != undefined && item[eachKey] != null) {
            const itemValue = item[eachKey].toLowerCase();
            const filterValue = filterargs[eachKey].toLowerCase();
            if (itemValue.indexOf(filterValue) > -1) {
              validFlag = true;
            }
          }
        });
        if (validFlag) {
          val.push(item);
        }
      });
      if (val.length) {
        this.orderList = [...val];
        this._loaderService.hide();
      }
      this._loaderService.hide();
    } else {
      this.orderList = [...this.orderListCopy];
      this._loaderService.hide();
    }

  }
  onWriteComments(orderObj) {
    const modalRef = this._modalService.open(CommentComponent);
    modalRef.componentInstance.orderId = orderObj.OrderId;
    modalRef.componentInstance.userName = this.username;
    modalRef.componentInstance.userId = this.currentUser.User.UserId;
  }

  onTakePictures(orderObj) {
    this.router.navigate(['/take-picture/' + orderObj.OrderId]);
  }
}

