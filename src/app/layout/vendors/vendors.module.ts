import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AddVendorComponent } from './add-vendor/add-vendor.component';
import { VendorListComponent, CustomRendererVendorComponent } from './vendor-list/vendor-list.component';
import { LoaderComponent } from '../bs-component/components/loader/loader.component';
import { ViewVendorComponent } from './view-vendor/view-vendor.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { VendorUsersComponent } from './vendor-users/vendor-users.component';
import { CustomRendererComponent } from '../users/user-list/user-list.component';
import { AlwaysAuthGuard } from '../../shared/guard/auth.guard';
import { AdminGuard } from '../../shared/guard/admin.guard';
import { ManagerGuard } from '../../shared/guard/manager.guard';

const routes: Routes = [{
  path: '',
  children: [
    { path: 'add-vendor', component: AddVendorComponent },
    { path: 'edit-vendor/:id', component: AddVendorComponent },
    { path: 'view-vendor/:id', component: ViewVendorComponent },
    { path: 'vendor-list', component: VendorListComponent },
    { path: 'vendor-users/:id', component: VendorUsersComponent },
  ], canActivate: [AlwaysAuthGuard, AdminGuard, ManagerGuard]
}];

@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [AddVendorComponent, VendorListComponent, ViewVendorComponent, VendorUsersComponent, CustomRendererVendorComponent],
  entryComponents: [CustomRendererVendorComponent]
})
export class VendorsModule { }