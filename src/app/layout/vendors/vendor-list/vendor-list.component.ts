import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { Vendor } from '../../../model/vendor.model';
import { VendorService } from '../../../shared/services/vendor.service';
import { LoaderService } from '../../../shared/services/loader.service';
import { Router } from '@angular/router';
import { LocalDataSource, ViewCell } from 'ng2-smart-table';
import { ConfirmDialogComponent } from '../../bs-component/components/confirm-dialog/confirm-dialog.component';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-vendor-list',
  templateUrl: './vendor-list.component.html',
  styleUrls: ['./vendor-list.component.scss']
})
export class VendorListComponent implements OnInit {

  vendorsList: Vendor[];
  vendorsListCopy: Vendor[];
  source = new LocalDataSource();
  alphabets = [];
  perPageSelect: any;
  isPagerDisplay: boolean;
  alphabetSearch;
  settings = {
    actions: false,
    columns: {
      VendorName: {
        title: 'Name',
        type: 'custom',
        renderComponent: CustomRendererVendorComponent,
        filter: false
      },
      VendorPhone: {
        title: 'Vendor Phone',
        filter: false
      },
      Users: {
        title: 'View Users',
        type: 'custom',
        renderComponent: CustomRendererVendorComponent,
        valuePrepareFunction: (cell, usersList) => {
          return 'View Users';
        },
        filter: false
      },
      VendorID: {
        title: 'Action',
        type: 'custom',
        renderComponent: CustomRendererVendorComponent,
        filter: false
      },
      IsActive: {
        title: 'Block/Unblock',
        type: 'custom',
        renderComponent: CustomRendererVendorComponent,
        filter: false,
        onComponentInitFunction: (instance) => {
          instance.save.subscribe(row => {
            if (row) {
              this.getVendorsList();
            }
          });
        }
      }
    },
    pager: {
      display: true,
      perPage: 10,
    },
    hideSubHeader: true,
    sort: true,
  };
  constructor(
    private _vendorService: VendorService,
    private _loaderService: LoaderService,
    private _toasterService: ToastrService
  ) {
    this.vendorsList = [];
    this.vendorsListCopy = [];
  }

  ngOnInit() {
    this.getVendorsList();
    this.getAlphabets();
  }

  getVendorsList() {
    this._loaderService.show();
    this._vendorService.getAllVendors().subscribe(
      data => {
        if (data) {
          this.vendorsList = data;
          this.source.load(this.vendorsList);
          this._loaderService.hide();
        }
      }, error => {
        console.log(error);
        this._loaderService.hide();
      });
  }
  onSearch(query: string = '') {
    if (query) {
      this.source.setFilter([
        {
          field: 'VendorName',
          search: query
        },
        {
          field: 'VendorPhone',
          search: query
        },
      ], false, true);
    } else {
       if (this.alphabetSearch !== 'All') {
        this.source = new LocalDataSource(this.vendorsListCopy);
       } else {
        this.source = new LocalDataSource(this.vendorsList);
       }
    }
  }
  getAlphabets() {
    let i;
    for (i = 65; i <= 90; i++) {
      this.alphabets.push(String.fromCharCode(i));
    }
    if (i === 91) {
      this.alphabets.push('All');
    }
  }

  onAlphabetSearch(alphabet: any) {
    this.alphabetSearch = alphabet;
    this.vendorsListCopy = [];
    this._loaderService.show();
    let flag = false;
    if (alphabet === 'All') {
      this.source = new LocalDataSource(this.vendorsList);
      this._loaderService.hide();
    } else {
      this.vendorsList.forEach((vendor, index) => {
        if (vendor.VendorName.startsWith(alphabet, 0)) {
          this.vendorsListCopy.push(vendor);
        }
        if (index === this.vendorsList.length - 1) {
          flag = true;
        }
      });
      if (flag) {
        this.source = new LocalDataSource(this.vendorsListCopy);
        this._loaderService.hide();
      } else {
        this._loaderService.hide();
        this._toasterService.error('Error in Loading Vendor List');
      }
    }
  }
}
@Component({
  template: `
    <a class="btn btn-link font-text" (click)="navigateToSomeRoute()">{{title}}</a>
  `,
  styleUrls: ['./vendor-list.component.scss']
})

export class CustomRendererVendorComponent implements ViewCell, OnInit {
  value: string | number;
  rowData: any;
  title: any;
  flagBlock: boolean;
  constructor(private router: Router, private _modalService: NgbModal,
    private _vendorService: VendorService,
    private _loaderService: LoaderService,
    private _toasterService: ToastrService
  ) {
    this.flagBlock = false;
  }
  renderValue: string;
  @Output() save: EventEmitter<any> = new EventEmitter();

  ngOnInit() {
    if (typeof (this.value) === 'number') {
      this.title = 'Edit';
    } else if (typeof (this.value) === 'string') {
      if (this.value === 'View Users') {
        this.title = this.value;
      } else {
        this.title = this.value;
      }
    } else if (this.value === true) {
      this.title = 'Block';
      this.flagBlock = true;
    } else if (this.value === false) {
      this.title = 'UnBlock';
      this.flagBlock = false;
    } else {
      this.title = 'Block'; // comment else part if isactive implemented on server side
    }
  }

  navigateToSomeRoute() {
    if (typeof (this.value) === 'number') {
      this.router.navigate(['/vendor/edit-vendor/' + this.value]);
    } else if (typeof (this.value) === 'string') {
      if (this.value === 'View Users') {
        this.router.navigate(['/vendor/vendor-users/' + this.rowData.VendorID]);
      } else {
        this.router.navigate(['/vendor/view-vendor/' + this.rowData.VendorID]);
      }
    } else {
      const modalRef = this._modalService.open(ConfirmDialogComponent);
      modalRef.componentInstance.titleFrom = 'Confirm Block/Unblock ';
      modalRef.componentInstance.message = 'Are You Sure ? Do you want to ' + this.title + ' '
        + this.rowData.VendorName + ' ?';
      modalRef.result
        .then((result) => {
          if (result) {
            const id = this.rowData.VendorID;
            if (this.flagBlock === true) {
              this.blockVendor(id);
            } else {
              this.unblockVendor(id);
            }
          }
        });
    }
  }
  blockVendor(id: any) {
    this._loaderService.show();
    this._vendorService.blockVendor(id).subscribe(
      data => {
        this._loaderService.hide();
        if (data === true) {
          this.save.emit('true');
          this._toasterService.success(this.rowData.VendorName + ' is Blocked Successfully!');
        } else {
          this._toasterService.error('Server Error Occurred!!');
        }
      }, error => {
        this._loaderService.hide();
        this._toasterService.error('Server Error Occurred!!');
      });
  }
  unblockVendor(id: any) {
    this._loaderService.show();
    this._vendorService.unblockVendor(id).subscribe(
      data => {
        this._loaderService.hide();
        if (data === true) {
          this.save.emit('true');
          this._toasterService.success(this.rowData.VendorName + ' is UnBlocked Successfully!');
        } else {
          this._toasterService.error('Server Error Occurred!!');
        }
      }, error => {
        this._loaderService.hide();
        this._toasterService.error('Server Error Occurred!!');
      });
  }
}

