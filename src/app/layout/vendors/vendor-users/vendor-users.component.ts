import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VendorService } from '../../../shared/services/vendor.service';
import { LoaderService } from '../../../shared/services/loader.service';
import { User } from '../../../model/user.model';
import { Vendor } from '../../../model/vendor.model';
import { LocalDataSource } from 'ng2-smart-table';
import { CustomRendererComponent } from '../../users/user-list/user-list.component';

@Component({
  selector: 'app-vendor-users',
  templateUrl: './vendor-users.component.html',
  styleUrls: ['./vendor-users.component.scss']
})
export class VendorUsersComponent implements OnInit {
  //  usersList: User[];
  vendorModal: Vendor;
  id: any;
  source = new LocalDataSource(); // add a property to the component
  settings = {
    actions: false,
    columns: {
      FirstName: {
        title: 'Name',
        type: 'custom',
        valuePrepareFunction: (cell, usersList) => {
          return usersList.FirstName + ' ' + usersList.LastName;
        },
        renderComponent: CustomRendererComponent,
        filter: false
      },
      Email: {
        title: 'Email',
        filter: false
      },
      Phone: {
        title: 'Phone',
        filter: false
      },
      UserId: {
        title: 'Action',
        type: 'custom',
        // valuePrepareFunction: (cell, vendorUserList) => {
        //   return { 'userId': vendorUserList.UserId, 'vendorId': this.id };
        // },
        renderComponent: CustomRendererComponent,
        filter: false
      },
      IsActive: {
        title: 'Block/Unblock',
        type: 'custom',
        filter: false,
        renderComponent: CustomRendererComponent,
        onComponentInitFunction: (instance) => {
          instance.save.subscribe(row => {
            if (row) {
              this.getVendorWithUsers();
            }
          });
        }
      }
    },
    pager: {
      display: true,
      perPage: 20,
    },
    hideSubHeader: true,
    sort: true,
  };
  constructor(
    private route: ActivatedRoute,
    private _vendorService: VendorService,
    private _loaderService: LoaderService,
  ) {
    //  this.usersList = [];
    this.vendorModal = new Vendor();
  }

  ngOnInit() {
    this.getVendorWithUsers();
  }
  getVendorWithUsers() {
    this._loaderService.show();
    this.id = this.route.snapshot.paramMap.get('id');
    this._vendorService.getVendorByID(this.id).subscribe(
      data => {
        this._loaderService.hide();
        if (data) {
          this.vendorModal = data;
          // this.usersList = this.vendorModal.Users;
          this.source = new LocalDataSource(this.vendorModal.Users);
        }
      }, error => {
        this._loaderService.hide();
        console.log(error);
      });
  }
  onSearch(query: string = '') {
    console.log('query', query);
    if (query) {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'FirstName',
          search: query
        },
        {
          field: 'LastName',
          search: query
        },
        {
          field: 'Email',
          search: query
        },
        {
          field: 'Phone',
          search: query
        }
      ], false, true);
    } else {
      this.source = new LocalDataSource(this.vendorModal.Users);
    }
  }

}
