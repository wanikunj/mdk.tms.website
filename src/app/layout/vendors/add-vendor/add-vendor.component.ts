import { Component, OnInit, OnDestroy } from '@angular/core';
import { Vendor } from '../../../model/vendor.model';
import { NgForm } from '@angular/forms';
import { LoaderService } from '../../../shared/services/loader.service';
import { VendorService } from '../../../shared/services/vendor.service';
import { CommonService } from '../../../shared/services/common.service';
import { routerTransition } from '../../../router.animations';
import { ActivatedRoute, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs';
import { UserLogin } from '../../../model/login-user.model';
import { GuardService } from '../../../shared/services/guard.service';


@Component({
  selector: 'app-add-vendor',
  templateUrl: './add-vendor.component.html',
  styleUrls: ['./add-vendor.component.scss'],
  animations: [routerTransition()]
})
export class AddVendorComponent implements OnInit, OnDestroy {

  vendor: any = {};
  subBtnName: string;
  topHeader: string;
  user$: Subscription;
  currentUser: UserLogin;
  userId: any;
  constructor(
    private _loaderService: LoaderService,
    private _vendorService: VendorService,
    private _commonService: CommonService,
    private route: ActivatedRoute,
    private router: Router,
    private _guard: GuardService,
    private _toasterService: ToastrService
  ) {
    this.vendor = new Vendor();
    this.subBtnName = 'Add Vendor';
    this.topHeader = 'Add New Vendor';
  }

  ngOnInit() {
    this.user$ = this._guard.user$.subscribe(user => {
      this.currentUser = user;
      if (this.currentUser.User != undefined || this.currentUser.User !== null) {
        this.userId = this.currentUser.User.UserId;
      }
    });
    const id = this.route.snapshot.paramMap.get('id');
    if (id != null) {
      this.subBtnName = 'Update Vendor';
      this.topHeader = 'Edit Vendor';
      this._loaderService.show();
      this._vendorService.getVendorByID(id).subscribe(
        data => {
          if (data) {
            this.vendor = data;
            this._loaderService.hide();
          }
        }, error => {
          console.log(error);
          this._loaderService.hide();
        });
    } else {
      this.subBtnName = 'Add Vendor';
      this.topHeader = 'Add New Vendor';
    }
  }

  ngOnDestroy() {
    this.user$.unsubscribe();
  }
  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this._loaderService.show();
      const file = event.target.files[0];
      console.log(file);
      const fileReader = new FileReader();
      fileReader.onloadend = (e) => {
        const rawData = fileReader.result;
        this._loaderService.hide();
        this.vendor.VendorLogo = rawData.toString();
        // console.log(rawData);
      };
      fileReader.readAsDataURL(file);
    }
  }

  onSubmit(form: NgForm) {
    this._loaderService.show();
    if (this.vendor.VendorID > 0) {
      this.updateVendor();
    } else {
      this.addVendor(form);
    }
  }
  addVendor(form: NgForm) {
    this.vendor.TimeOffSet = this._commonService.getUserTimeZoneOffSet();
    this.vendor.CreatedBy = this.currentUser.User.UserId;
    this.vendor.UpdatedBy = this.currentUser.User.UserId;
    this.vendor.CreatedDate = new Date(Date.now());
    this._vendorService.addVendor(this.vendor).subscribe(
      data => {
        if (data) {
          this._loaderService.hide();
          if (data > 0) {
            this._toasterService.success('Vendor Added Successfully!!');
            form.resetForm();
          } else {
            this._toasterService.error('Vendor Not Added');
          }
        }
      }, error => {
        this._loaderService.hide();
        this._toasterService.error('Server Error Occurred');
      });
  }
  updateVendor() {
    this.vendor.TimeOffSet = this._commonService.getUserTimeZoneOffSet();
    this.vendor.UpdatedBy = this.currentUser.User.UserId;
    this._vendorService.updateVendor(this.vendor).subscribe(
      data => {
        if (data) {
          this._loaderService.hide();
          if (data === true) {
            this._toasterService.success('Vendor Updated Successfully!!');
            console.log('Success');
            this.router.navigate(['/vendor/vendor-list']);
          } else {
            console.log('Error');
            this._toasterService.error('Vendor Not Updated');
          }
        }
      }, error => {
        this._loaderService.hide();
        this._toasterService.error('Server Error Occurred');
      });
  }
}
