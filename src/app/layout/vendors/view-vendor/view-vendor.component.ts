import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { ActivatedRoute } from '@angular/router';
import { LoaderService } from '../../../shared/services/loader.service';
import { Vendor } from '../../../model/vendor.model';
import { VendorService } from '../../../shared/services/vendor.service';

@Component({
  selector: 'app-view-vendor',
  templateUrl: './view-vendor.component.html',
  styleUrls: ['./view-vendor.component.scss'],
  animations: [routerTransition()]
})
export class ViewVendorComponent implements OnInit {
  vendorModal: Vendor;
  constructor(
    private route: ActivatedRoute,
    private _vendorService: VendorService,
    private _loaderService: LoaderService,
  ) {
    this.vendorModal = new Vendor();
  }

  ngOnInit() {
    this.getVendor();
  }
  getVendor() {
    this._loaderService.show();
    const id = this.route.snapshot.paramMap.get('id');
    this._vendorService.getVendorByID(id).subscribe(
      data => {
        if (data) {
          this.vendorModal = data;
          this._loaderService.hide();
          console.log(data);
        }
      }, error => {
        this._loaderService.hide();
        console.log(error);
      });
  }
}
