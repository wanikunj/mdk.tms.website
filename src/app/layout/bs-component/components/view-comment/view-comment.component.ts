import { Component, OnInit, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from '../../../../shared/services/loader.service';
import { CommentService } from '../../../../shared/services/comment.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { OrderComments } from '../../../../model/comment.model';

@Component({
  selector: 'app-view-comment',
  templateUrl: './view-comment.component.html',
  styleUrls: ['./view-comment.component.scss']
})
export class ViewCommentComponent implements OnInit {

  @Input() orderId;
  orderComments: OrderComments[];
  bgClass = [];
  color = ['primary', 'warning', 'success', 'info'];
  constructor(public activeModal: NgbActiveModal, private _toasterService: ToastrService, private _loaderService: LoaderService,
    private _commentService: CommentService) { }

  ngOnInit() {
    this.getCommentById();
  }

  getCommentById() {
    this._loaderService.show();
    this._commentService.getOrderComments(this.orderId).subscribe((data) => {
      if (data) {
        this.orderComments = data;
        let i = 0;
        this.orderComments.forEach((ele, index) => {
          if (i < 4) {
            this.bgClass.push(this.color[i]);
            i = i + 1;
          } else {
            i = 0;
            this.bgClass.push(this.color[i]);
            i = i + 1;
          }
        });
        this._loaderService.hide();
      }
      this._loaderService.hide();
    }, (error) => {
      this._loaderService.hide();
      this._toasterService.error('Server Error Occurred!');
    });
  }
}
