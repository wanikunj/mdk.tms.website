import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { LoaderService } from '../../../../shared/services/loader.service';
import { ToastrService } from 'ngx-toastr';
import { InvoiceService } from '../../../../shared/services/invoice.service';
import { CommonService } from '../../../../shared/services/common.service';
import { Invoice, UpdateInvoiceReq } from '../../../../model/invoice.model';
import { GuardService } from '../../../../shared/services/guard.service';
import { Subscription } from 'rxjs';
import { UserLogin } from '../../../../model/login-user.model';

@Component({
  selector: 'app-invoice-status',
  templateUrl: './invoice-status.component.html',
  styleUrls: ['./invoice-status.component.scss']
})
export class InvoiceStatusComponent implements OnInit, OnDestroy {

  invoiceStatusId: any;
  invoiceStatusList = [
    { 'ID': '1', 'Value': 'Unpaid' },
    { 'ID': '2', 'Value': 'Paid' },
    { 'ID': '3', 'Value': 'Partially Paid' }];
  @Input() invoiceObj: Invoice;
  copyInvoiceObj: Invoice;
  updateInvoiceReq: UpdateInvoiceReq;
  amount;
  userId;
  user$: Subscription;
  currentUser: UserLogin;

  constructor(public activeModal: NgbActiveModal, private loaderService: LoaderService,
    private _commonService: CommonService, private _guard: GuardService,
    private _toasterServcie: ToastrService, private _invoiceService: InvoiceService) {
    this.updateInvoiceReq = new UpdateInvoiceReq();
  }

  ngOnInit() {
    this.user$ = this._guard.user$.subscribe(user => {
      this.currentUser = user;
      if (this.currentUser.User != null) {
        if (this.currentUser.Vendors != null) {
          this.userId = this.currentUser.Vendors.VendorID;
        } else {
          this.userId = this.currentUser.User.UserId;
        }
      }
    });
    this.copyInvoiceObj = this.invoiceObj;
    this.amount = this.invoiceObj.AmountDue;
    this.invoiceStatusList.forEach((value) => {
      if (value.Value == this.invoiceObj.InvoiceStatusName) {
        this.invoiceStatusId = value.ID;
      }
    });
  }

  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;

  }
  ngOnDestroy() {
    this.user$.unsubscribe();
  }
  onSelect(value) {
    this.invoiceStatusList.forEach((ele) => {
      if (ele.ID == value) {
        this.invoiceStatusId = ele.ID;
      }
    });
  }
  onUpdate() {
    if (this.copyInvoiceObj.InvoiceStatusID == this.invoiceStatusId) {
      this._toasterServcie.warning('Please Change the payment status to Update.');
    } else {
      this.loaderService.show();
      this.updateInvoiceReq.TimeOffset = this._commonService.getUserTimeZoneOffSet();
      this.updateInvoiceReq.Amount = this.amount;
      this.updateInvoiceReq.InvoiceID = this.invoiceObj.InvoiceID;
      this.updateInvoiceReq.StatusID = this.invoiceStatusId;
      this.updateInvoiceReq.UserID = this.currentUser.User.UserId;
      this._invoiceService.updateInvoiceStatus(this.updateInvoiceReq).subscribe((data) => {
        if (data) {
          this.loaderService.hide();
          this._toasterServcie.success('Invoice Updated Successfully!');
          this.activeModal.close(true);
        } else {
          this.loaderService.hide();
          this._toasterServcie.error('Invoice status not Updated !!');
          this.activeModal.close(false);
        }
      }, error => {
        this.loaderService.hide();
        this.activeModal.close(false);
        this._toasterServcie.error('Server Error occured !!');
      });
    }
  }
}
