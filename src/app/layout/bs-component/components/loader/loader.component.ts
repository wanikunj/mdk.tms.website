import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { LoaderState } from '../../../../model/loader-state';
import { LoaderService } from '../../../../shared/services/loader.service';

@Component({
    selector: 'app-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.scss']
})
export class LoaderComponent implements OnInit, OnDestroy {

    show: Boolean = false;
    private subscription: Subscription;
    constructor(private loaderService: LoaderService) {

    }
    ngOnInit() {
        this.subscription = this.loaderService.loaderState
            .subscribe((state: LoaderState) => {
                this.show = state.show;
            });
    }


    ngOnDestroy() {
        this.subscription.unsubscribe();
    }

}
