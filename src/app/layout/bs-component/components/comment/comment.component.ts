import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { OrderComments } from '../../../../model/comment.model';
import { CommonService } from '../../../../shared/services/common.service';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from '../../../../shared/services/loader.service';
import { CommentService } from '../../../../shared/services/comment.service';
import { OrderDetail } from '../../../../model/driver.model';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {
  comment = '';
  @Input() orderId;
  @Input() userName;
  @Input() userId;
  orderComments: OrderComments;
  constructor(public activeModal: NgbActiveModal, private _commonService: CommonService,
    private _toasterService: ToastrService, private _loaderService: LoaderService,
    private _commentService: CommentService) {
    this.orderComments = new OrderComments();
  }

  ngOnInit() {
  }

  onSave() {
    this._loaderService.show();
    this.orderComments.TimeOffset = this._commonService.getUserTimeZoneOffSet();
    this.orderComments.Comment = this.comment;
    this.orderComments.OrderID = this.orderId;
    this.orderComments.UserName = this.userName;
    this.orderComments.CommentsBy = this.userId;
    this._commentService.addOrderComments(this.orderComments).subscribe((data) => {
      if (data) {
        this._toasterService.success('Comment Added Successfully!');
        this._loaderService.hide();
        this.activeModal.close();
      } else {
        this._toasterService.error('Comment not Added Successfully!!');
        this.activeModal.close();
      }
      this._loaderService.hide();
    }, (error) => {
      this._loaderService.hide();
      this.activeModal.close();
      this._toasterService.error('Server Error Occurred');
    });
  }
}
