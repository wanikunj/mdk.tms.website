import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { HelperClass } from '../../../../model/vendor-order.model';

@Component({
  selector: 'app-change-status',
  templateUrl: './change-status.component.html',
  styleUrls: ['./change-status.component.scss']
})
export class ChangeStatusComponent implements OnInit {

  public title: any = '';
  @Input() titleFrom;
  @Input() statusValue;
  // orderStatusId;
  orderStatusId = { 'ID': '', 'Value': '' };
  orderStatusList = [{ 'ID': '1', 'Value': 'Pending' },
  { 'ID': '2', 'Value': 'Created' },
  { 'ID': '3', 'Value': 'Picked' },
  { 'ID': '4', 'Value': 'Delivered' },
  { 'ID': '5', 'Value': 'Received at port' }];

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
    if (this.titleFrom === 'Payment Status') {
      this.getPaymentStatusList();
    } else {
      this.orderStatusList.filter((value) => {
        if (value.ID == this.statusValue) {
          console.log('value', value);
          this.orderStatusId = value;
          console.log('orderStatusId', this.orderStatusId);
        }
      });
    }
  }

  getPaymentStatusList() {
  }
}
