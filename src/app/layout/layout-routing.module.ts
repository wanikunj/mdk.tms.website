import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from './layout.component';
import { PlaceNewOrderComponent } from './orders/place-new-order/place-new-order.component';

const routes: Routes = [
    {
        path: '',
        component: LayoutComponent,
        children: [
            { path: '', redirectTo: 'dashboard', pathMatch: 'prefix' },
            { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
            { path: 'charts', loadChildren: './charts/charts.module#ChartsModule' },
            { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
            { path: 'forms', loadChildren: './form/form.module#FormModule' },
            { path: 'bs-element', loadChildren: './bs-element/bs-element.module#BsElementModule' },
            { path: 'grid', loadChildren: './grid/grid.module#GridModule' },
            { path: 'components', loadChildren: './bs-component/bs-component.module#BsComponentModule' },
            { path: 'blank-page', loadChildren: './blank-page/blank-page.module#BlankPageModule' },
            { path: 'neworders', loadChildren: './orders/orders.module#OrdersModule' },
            { path: 'user', loadChildren: './users/users.module#UsersModule' },
            { path: 'vendor', loadChildren: './vendors/vendors.module#VendorsModule' },
            { path: 'profile/:id', loadChildren: './profile/profile.module#ProfileModule' },
            { path: 'invoice', loadChildren: './invoices/invoices.module#InvoicesModule' },
        ]
    },
    // {path:'neworders',component:PlaceNewOrderComponent},
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class LayoutRoutingModule { }
