import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { UserLogin } from '../../../model/login-user.model';
import { GuardService } from '../../../shared/services/guard.service';
import { AuthService } from '../../../shared/services/auth.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
    pushRightClass: any = 'push-right';
    user$: Subscription;
    currentUser: UserLogin;
    username: any = '';

    constructor(private translate: TranslateService, public router: Router, private _guard: GuardService,
        private _authService: AuthService) {
        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    ngOnInit() {
        this.user$ = this._guard.user$.subscribe(user => {
            this.currentUser = user;
            if (this.currentUser.User != null) {
                this.username = this.currentUser.User.FirstName + ' ' + this.currentUser.User.LastName;
            } else {
                this.router.navigate(['/login']);
            }
        });
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    rltAndLtr() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle('rtl');
    }

    onLoggedout() {
        this._authService.logout();
        localStorage.removeItem('isLoggedin');
        // this._guard.logoutCurrentUser();
    }

    changeLang(language: string) {
        this.translate.use(language);
    }

    ngOnDestroy() {
        this.user$.unsubscribe();
    }
    onProfile() {
        this.router.navigate(['/profile/' + this.currentUser.User.UserId]);
    }
}
