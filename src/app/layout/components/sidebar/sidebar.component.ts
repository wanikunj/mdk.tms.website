import { Component, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { Subscription } from 'rxjs';
import { UserLogin } from '../../../model/login-user.model';
import { GuardService } from '../../../shared/services/guard.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { AuthService } from '../../../shared/services/auth.service';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
    styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnDestroy {
    isActive: boolean = false;
    collapsed: boolean = false;
    showMenu: string = '';
    pushRightClass: string = 'push-right';
    user$: Subscription;
    currentUser: UserLogin;
    username = '';

    @Output() collapsedEvent = new EventEmitter<boolean>();

    constructor(private translate: TranslateService,
        private _authService: AuthService, public router: Router, private _guard: GuardService) {
        this.router.events.subscribe(val => {
            if (
                val instanceof NavigationEnd &&
                window.innerWidth <= 992 &&
                this.isToggled()
            ) {
                this.toggleSidebar();
            }
        });
    }

    eventCalled() {
        this.isActive = !this.isActive;
    }

    ngOnInit() {
        this.user$ = this._guard.user$.subscribe(user => {
            this.currentUser = user;
            if (this.currentUser.User != undefined || this.currentUser.User !== null) {
                console.log('this debugger', this.currentUser);
                this.username = this.currentUser.User.FirstName + ' ' + this.currentUser.User.LastName;
                if (this.currentUser.User.UserType != '') {
                    if (this.currentUser.User.UserType === 'Admin' || this.currentUser.User.UserType === 'Manager') {
                        this.currentUser.isAdmin = true;
                        this.currentUser.isManager = true;
                    } else if (this.currentUser.User.UserType === 'Driver') {
                        this.router.navigate(['/dashboard/driver-dashboard']);
                        this.currentUser.isDriver = true;
                    } else if (this.currentUser.User.UserType === 'Local Port Employee') {
                        this.router.navigate(['/dashboard/port-employee']);
                        this.currentUser.isLocalPortEmployee = true;
                    } else if (this.currentUser.User.UserType === 'Vendor') {
                        this.router.navigate(['/dashboard/vendor-dashboard']);
                        this.currentUser.isVendor = true;
                    }
                } else {
                    // this._guard.logoutCurrentUser();
                    this.router.navigate(['/login']);
                }
            } else {
                // this._guard.logoutCurrentUser();
                this.router.navigate(['/login']);
            }
        });
    }
    addExpandClass(element: any) {
        if (element === this.showMenu) {
            this.showMenu = '0';
        } else {
            this.showMenu = element;
        }
    }

    toggleCollapsed() {
        this.collapsed = !this.collapsed;
        this.collapsedEvent.emit(this.collapsed);
    }

    isToggled(): boolean {
        const dom: Element = document.querySelector('body');
        return dom.classList.contains(this.pushRightClass);
    }

    toggleSidebar() {
        const dom: any = document.querySelector('body');
        dom.classList.toggle(this.pushRightClass);
    }

    onLoggedout() {
        this._authService.logout();
        localStorage.removeItem('isLoggedin');
    }

    ngOnDestroy() {
        this.user$.unsubscribe();
    }
    onProfile() {
        this.router.navigate(['/profile/' + this.currentUser.User.UserId]);
    }
    onVendorInvoice() {
        if (this.currentUser.Vendors != null) {
            this.router.navigate(['/invoice/view-invoice-list/' + this.currentUser.Vendors.VendorID]);
        }
    }
}
