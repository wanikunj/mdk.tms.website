import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CreateInvoiceComponent } from './create-invoice/create-invoice.component';
import { ViewInvoiceComponent } from './view-invoice/view-invoice.component';
import { InvoiceListComponent, CustomRendererInvoiceComponent } from './invoice-list/invoice-list.component';
import { Routes, RouterModule } from '@angular/router';
import { AlwaysAuthGuard } from '../../shared/guard/auth.guard';
import { VendorGuard } from '../../shared/guard/vendor.guard';
import { SharedModule } from '../../shared/modules/shared.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AdminGuard } from '../../shared/guard/admin.guard';
import { ManagerGuard } from '../../shared/guard/manager.guard';
import { ViewInvoiceListComponent, CustomRendererInvoiceListComponent } from './view-invoice-list/view-invoice-list.component';

const routes: Routes = [{
  path: '',
  children: [
    { path: 'invoice-list', component: InvoiceListComponent },
    { path: 'create-invoice/:id', component: CreateInvoiceComponent },
    { path: 'invoice-view/:invoiceId/:vendorId', component: ViewInvoiceComponent },
    { path: 'view-invoice-list/:id', component: ViewInvoiceListComponent },
  ], canActivate: [AlwaysAuthGuard]
}];
@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    RouterModule.forChild(routes),
    SharedModule
  ],
  declarations: [CreateInvoiceComponent, ViewInvoiceComponent, InvoiceListComponent,
    CustomRendererInvoiceComponent, ViewInvoiceListComponent, CustomRendererInvoiceListComponent],
  entryComponents: [CustomRendererInvoiceComponent, CustomRendererInvoiceListComponent]
})
export class InvoicesModule { }
