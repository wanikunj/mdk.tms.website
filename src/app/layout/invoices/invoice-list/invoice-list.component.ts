import { Component, OnInit } from '@angular/core';
import { LocalDataSource, ViewCell } from 'ng2-smart-table';
import { VendorService } from '../../../shared/services/vendor.service';
import { LoaderService } from '../../../shared/services/loader.service';
import { ToastrService } from 'ngx-toastr';
import { Vendor } from '../../../model/vendor.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-invoice-list',
  templateUrl: './invoice-list.component.html',
  styleUrls: ['./invoice-list.component.scss']
})
export class InvoiceListComponent implements OnInit {


  vendorsList: Vendor[];
  vendorsListCopy: Vendor[];
  source = new LocalDataSource();
  alphabets = [];
  perPageSelect: any;
  isPagerDisplay: boolean;
  alphabetSearch;
  settings = {
    actions: false,
    columns: {
      VendorName: {
        title: 'Name',
        filter: false
      },
      Users: {
        title: 'Create Invoice',
        type: 'custom',
        renderComponent: CustomRendererInvoiceComponent,
        valuePrepareFunction: (cell, invoiceList) => {
          return 'Create Invoice';
        },
        filter: false
      },
      VendorID: {
        title: 'View Invoice',
        type: 'custom',
        renderComponent: CustomRendererInvoiceComponent,
        valuePrepareFunction: (cell, invoiceList) => {
          return 'View Invoice';
        },
        filter: false
      }
    },
    pager: {
      display: true,
      perPage: 20,
    },
    hideSubHeader: true,
    sort: true,
  };
  constructor(
    private _vendorService: VendorService,
    private _loaderService: LoaderService,
    private _toasterService: ToastrService
  ) {
    this.vendorsList = [];
    this.vendorsListCopy = [];
  }

  ngOnInit() {
    this.getInvoiceList();
    this.getAlphabets();
  }

  getInvoiceList() {
    this._loaderService.show();
    this._vendorService.getAllVendors().subscribe(
      data => {
        if (data) {
          this.vendorsList = data;
          this.source.load(this.vendorsList);
          this._loaderService.hide();
        }
      }, error => {
        console.log(error);
        this._loaderService.hide();
      });
  }
  onSearch(query: string = '') {
    if (query) {
      this.source.setFilter([
        {
          field: 'VendorName',
          search: query
        },
        {
          field: 'VendorPhone',
          search: query
        },
      ], false, true);
    } else {
      if (this.alphabetSearch !== 'All') {
        this.source = new LocalDataSource(this.vendorsListCopy);
      } else {
        this.source = new LocalDataSource(this.vendorsList);
      }
    }
  }
  getAlphabets() {
    let i;
    for (i = 65; i <= 90; i++) {
      this.alphabets.push(String.fromCharCode(i));
    }
    if (i === 91) {
      this.alphabets.push('All');
    }
  }

  onAlphabetSearch(alphabet: any) {
    this.alphabetSearch = alphabet;
    this.vendorsListCopy = [];
    this._loaderService.show();
    let flag = false;
    if (alphabet === 'All') {
      this.source = new LocalDataSource(this.vendorsList);
      this._loaderService.hide();
    } else {
      this.vendorsList.forEach((vendor, index) => {
        if (vendor.VendorName.startsWith(alphabet, 0)) {
          this.vendorsListCopy.push(vendor);
        }
        if (index === this.vendorsList.length - 1) {
          flag = true;
        }
      });
      if (flag) {
        this.source = new LocalDataSource(this.vendorsListCopy);
        this._loaderService.hide();
      } else {
        this._loaderService.hide();
        this._toasterService.error('Error in Loading Invoice List');
      }
    }
  }
}
@Component({
  template: `
    <a class="btn btn-link font-text" (click)="navigateToSomeRoute()">{{title}}</a>
  `,
  styleUrls: ['./invoice-list.component.scss']
})

export class CustomRendererInvoiceComponent implements ViewCell, OnInit {
  value: string | number;
  rowData: any;
  title: any;
  flagBlock: boolean;
  constructor(private router: Router) { }
  renderValue: string;
  id;

  ngOnInit() {
    this.id = this.rowData.VendorID;
    if (this.value === 'Create Invoice') {
      this.title = this.value;
    } else if (this.value === 'View Invoice') {
      this.title = this.value;
    }
  }

  navigateToSomeRoute() {
    if (this.value === 'Create Invoice') {
      this.router.navigate(['/invoice/create-invoice/' + this.id]);
    } else if (this.value === 'View Invoice') {
      this.router.navigate(['/invoice/view-invoice-list/' + this.id]);
    }
  }
}
