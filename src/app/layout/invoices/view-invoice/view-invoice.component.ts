import { Component, OnInit } from '@angular/core';
import { routerTransition } from '../../../router.animations';
import { Router, ActivatedRoute } from '@angular/router';
import { LoaderService } from '../../../shared/services/loader.service';
import { ToastrService } from 'ngx-toastr';
import { GuardService } from '../../../shared/services/guard.service';
import { VendorService } from '../../../shared/services/vendor.service';
import { InvoiceService } from '../../../shared/services/invoice.service';
import { Vendor } from '../../../model/vendor.model';
import { LocalDataSource } from 'ng2-smart-table';
import { InvoiceOrders, Invoice } from '../../../model/invoice.model';
import { Location } from '@angular/common';

@Component({
  selector: 'app-view-invoice',
  templateUrl: './view-invoice.component.html',
  styleUrls: ['./view-invoice.component.scss'],
  animations: [routerTransition()]

})
export class ViewInvoiceComponent implements OnInit {

  vendorId: any;
  invoiceId: any;
  vendorModal: Vendor;
  source = new LocalDataSource();
  invoiceOrderList: InvoiceOrders[];
  invoice: Invoice;
  settings = {
    actions: false,
    pager: false,
    columns: {
      ID: {
        title: 'No',
        filter: false
      },
      OrderDate: {
        title: 'Order Date',
        filter: false,
      },
      AuctionName: {
        title: 'Auction Name',
        filter: false,
      },
      VehicleName: {
        title: 'Vehicle Name',
        filter: false
      },
      Chasis: {
        title: 'Chassis No.',
        filter: false,
      },
      Price: {
        title: 'Price (¥)',
        filter: false,
      }
    },
    hideSubHeader: true,
    sort: true,
  };
  constructor(private _router: Router, private _loaderService: LoaderService,
    private _toasterService: ToastrService, private _location: Location,
    private _guard: GuardService, private _route: ActivatedRoute, private _vendorService: VendorService,
    private _invoiceService: InvoiceService) {
    this.invoice = new Invoice();
    this.vendorModal = new Vendor();
  }

  ngOnInit() {
    this._loaderService.show();
    this.vendorId = this._route.snapshot.paramMap.get('vendorId');
    this.invoiceId = this._route.snapshot.paramMap.get('invoiceId');
    this.getVendorById();
    this.getInvoiceList();
  }

  getVendorById() {
    this._loaderService.show();
    this._vendorService.getVendorByID(this.vendorId).subscribe(
      data => {
        if (data) {
          this.vendorModal = data;
          this._loaderService.hide();
        }
        this._loaderService.hide();
      }, error => {
        this._loaderService.hide();
      });
  }

  getInvoiceList() {
    this._loaderService.show();
    this._invoiceService.getInvoiceById(this.invoiceId).subscribe(
      data => {
        this._loaderService.hide();
        if (data) {
          this.invoice = data;
          this.invoiceOrderList = data.InvoiceOrders;
          this.source = new LocalDataSource(this.invoiceOrderList);
        }
      }, error => {
        this._loaderService.hide();
      });
  }
  onPrint() {
    window.print();
  }
  onOk() {
    this._location.back();
  }
}
