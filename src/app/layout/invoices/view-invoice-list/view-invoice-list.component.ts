import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { InvoiceService } from '../../../shared/services/invoice.service';
import { ToastrService } from 'ngx-toastr';
import { LoaderService } from '../../../shared/services/loader.service';
import { LocalDataSource, ViewCell } from 'ng2-smart-table';
import { Invoice } from '../../../model/invoice.model';
import { Router, ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { InvoiceStatusComponent } from '../../bs-component/components/invoice-status/invoice-status.component';
import { ConfirmDialogComponent } from '../../bs-component/components/confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-view-invoice-list',
  templateUrl: './view-invoice-list.component.html',
  styleUrls: ['./view-invoice-list.component.scss']
})
export class ViewInvoiceListComponent implements OnInit {

  source = new LocalDataSource();
  invoiceList: Invoice[] = [];
  vendorId;
  settings = {
    actions: false,
    columns: {
      InvoiceID: {
        title: 'Invoice Number',
        filter: false
      },
      InvoiceDateTime: {
        title: 'Invoice Date',
        type: 'custom',
        renderComponent: CustomRendererInvoiceListComponent,
        valuePrepareFunction: (cell, invoiceList) => {
          return { 'value': invoiceList.InvoiceDateTime, 'title': 'InvoiceDateTime' };
        },
        filter: false
      },
      InvoiceDueDate: {
        title: 'Due Date',
        filter: false
      },
      TotalCost: {
        title: 'Amount Due',
        filter: false,
        valuePrepareFunction: (cell, invoiceList) => {
          return '¥ ' + invoiceList.TotalCost;
        },
      },
      InvoiceStatusName: {
        title: 'Status',
        type: 'custom',
        renderComponent: CustomRendererInvoiceListComponent,
        valuePrepareFunction: (cell, invoiceList) => {
          return { 'value': invoiceList.InvoiceStatusName, 'title': 'Status' };
        },
        onComponentInitFunction: (instance) => {
          instance.save.subscribe(row => {
            if (row) {
              this.getAllInvoiceList();
            }
          });
        },
        filter: false
      },
      AmountPaid: {
        title: 'Amount Paid',
        filter: false,
        valuePrepareFunction: (cell, invoiceList) => {
          return '¥ ' + invoiceList.AmountPaid;
        },
      },
      PaymentDate: {
        title: 'Payment Date',
        filter: false
      },
      VendorID: {
        title: 'Action',
        filter: false,
        type: 'custom',
        renderComponent: CustomRendererInvoiceListComponent,
        valuePrepareFunction: (cell, invoiceList) => {
          if (invoiceList.InvoiceStatusID == 1) {
            return { 'value': invoiceList.InvoiceID, 'title': 'Delete' };
          } else {
            return { 'value': invoiceList.InvoiceID, 'title': '' };
          }
        },
        onComponentInitFunction: (instance) => {
          instance.save.subscribe(row => {
            if (row) {
              this.getAllInvoiceList();
            }
          });
        },
      }
    },
    pager: {
      display: true,
      perPage: 20,
    },
    hideSubHeader: true,
    sort: true,
    rowClassFunction: (row) => {
      if (row.isSelected) {
        return this.changeColor(row);
      } else {
        return this.changeColor(row);
      }
    }
  };
  constructor(private _route: ActivatedRoute,
    private _invoiceService: InvoiceService,
    private _loaderService: LoaderService,
    private _toasterService: ToastrService
  ) {
  }
  changeColor(row) {
    if (row.data.InvoiceStatusName === 'Unpaid') {
      if (row.data.OverDueStatus) {
        return 'alert-danger';
      }
      return 'alert-success';
    } else if (row.data.InvoiceStatusName === 'Paid') {
      if (row.data.OverDueStatus) {
        return 'alert-danger';
      }
      return 'alert-secondary';
    } else if (row.data.InvoiceStatusName === 'Partially Paid') {
      if (row.data.OverDueStatus) {
        return 'alert-danger';
      }
      return 'alert-info';
    }
  }
  ngOnInit() {
    this._loaderService.show();
    this.vendorId = this._route.snapshot.paramMap.get('id');
    this.getAllInvoiceList();
  }

  getAllInvoiceList() {
    this._loaderService.show();
    this._invoiceService.getVendorInvoices(this.vendorId).subscribe(
      data => {
        if (data) {
          this.invoiceList = data;
          this.source.load(this.invoiceList);
          this._loaderService.hide();
        } else {
          this._loaderService.hide();
        }
      }, error => {
        console.log(error);
        this._loaderService.hide();
      });
  }

}

@Component({
  template: `
    <a class="btn btn-link font-text" *ngIf="this.value.title !== 'Delete'" (click)="navigateToSomeRoute()">{{title}}</a>
    <a class="btn btn-link font-text text-danger" *ngIf="this.value.title === 'Delete'" (click)="navigateToSomeRoute()">{{title}}</a>
  `,
  styleUrls: ['./view-invoice-list.component.scss']
})

export class CustomRendererInvoiceListComponent implements ViewCell, OnInit {
  value: any;
  rowData: any;
  title: any;
  flagBlock: boolean;
  @Output() save: EventEmitter<any> = new EventEmitter();
  constructor(private router: Router, private dialog: NgbModal, private _invoiceService: InvoiceService,
    private _loaderService: LoaderService, private _toasterService: ToastrService) { }
  renderValue: string;
  vendorId;

  ngOnInit() {
    this.vendorId = this.rowData.VendorID;
    if (this.value.title === 'InvoiceDateTime') {
      this.title = this.value.value;
    } else if (this.value.title === 'Status') {
      this.title = this.value.value;
    } else {
      this.title = this.value.title;
    }
  }

  navigateToSomeRoute() {
    if (this.value.title === 'InvoiceDateTime') {
      this.router.navigate(['/invoice/invoice-view/' + this.rowData.InvoiceID + '/' + this.vendorId]);
    } else if (this.value.title === 'Status') {
      const dialogRef = this.dialog.open(InvoiceStatusComponent);
      dialogRef.componentInstance.invoiceObj = this.rowData;
      dialogRef.result.then((result) => {
        if (result) {
          this.save.emit('true');
        }
      });
    } else {
      const modalRef = this.dialog.open(ConfirmDialogComponent);
      modalRef.componentInstance.titleFrom = 'Confirm Delete ';
      modalRef.componentInstance.message = 'Are You Sure ? Do you want to ' + this.title + ' this invoice ?';
      modalRef.result.then((result) => {
        if (result) {
          const id = this.rowData.InvoiceID;
          this._loaderService.show();
          this._invoiceService.deleteInvoice(id).subscribe(data => {
            this._loaderService.hide();
            if (data === true) {
              this.save.emit('true');
              this._toasterService.success('Invoice is Deleted Successfully!');
            } else {
              this._toasterService.error('Server Error Occurred!!');
            }
          }, error => {
            this._loaderService.hide();
            this._toasterService.error('Server Error Occurred!!');
          });
        }
      });
    }
  }
}
