import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewInvoiceListComponent } from './view-invoice-list.component';

describe('ViewInvoiceListComponent', () => {
  let component: ViewInvoiceListComponent;
  let fixture: ComponentFixture<ViewInvoiceListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewInvoiceListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewInvoiceListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
