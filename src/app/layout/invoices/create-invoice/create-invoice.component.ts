import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from '@angular/forms';
import { routerTransition } from '../../../router.animations';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { Ng2SmartTableComponent } from 'ng2-smart-table/ng2-smart-table.component';
import { LocalDataSource } from 'ng2-smart-table';
import { LoaderService } from '../../../shared/services/loader.service';
import { Subscription } from 'rxjs';
import { UserLogin } from '../../../model/login-user.model';
import { GuardService } from '../../../shared/services/guard.service';
import { Vendor } from '../../../model/vendor.model';
import { VendorService } from '../../../shared/services/vendor.service';
import { InvoiceOrderReq, InvoiceOrders, Invoice } from '../../../model/invoice.model';
import { InvoiceService } from '../../../shared/services/invoice.service';
import { CommonService } from '../../../shared/services/common.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-create-invoice',
  templateUrl: './create-invoice.component.html',
  styleUrls: ['./create-invoice.component.scss'],
  animations: [routerTransition()]
})
export class CreateInvoiceComponent implements OnInit, OnDestroy {

  startOrderDate: any;
  model: NgbDateStruct;
  modelend: NgbDateStruct;
  modeldue: NgbDateStruct;
  now = new Date();
  endOrderDate: any;
  invoiceOrderList: InvoiceOrders[];
  source = new LocalDataSource();
  invoiceOrderReq: InvoiceOrderReq;
  selectedItems: any[] = [];
  user$: Subscription;
  currentUser: UserLogin;
  userId: any;
  vendorId: any;
  vendorModal: Vendor;
  adjustments: number;
  currentTotal: number;
  totalPrice: number;
  dueDate: any;
  invoiceBody: Invoice;
  @ViewChild('table') smartTable: Ng2SmartTableComponent;

  selectedRow;
  settings = {
    actions: false,
    pager: false,
    selectMode: 'multi',
    columns: {
      VehicleName: {
        title: 'Vehicle Name',
        filter: false
      },
      Chasis: {
        title: 'Chassis No.',
        filter: false,
      },
      AuctionName: {
        title: 'Auction Name',
        filter: false,
      },
      OrderDate: {
        title: 'Order Date',
        filter: false,
      },
      // DeliveryDate: {
      //   title: 'Delivery Date',
      //   filter: false
      // },
      Price: {
        title: 'Price (¥)',
        filter: false,
      }
    },
    hideSubHeader: true,
    sort: true,
  };
  constructor(private _router: Router, private _loaderService: LoaderService,
    private _commonService: CommonService, private _toasterService: ToastrService,
    private _guard: GuardService, private _route: ActivatedRoute, private _vendorService: VendorService,
    private _invoiceService: InvoiceService) {
    this.vendorModal = new Vendor();
    this.invoiceOrderReq = new InvoiceOrderReq();
    this.currentTotal = 0;
    this.totalPrice = 0;
    this.invoiceBody = new Invoice();
  }

  ngOnInit() {
    this.user$ = this._guard.user$.subscribe(user => {
      this.currentUser = user;
      if (this.currentUser.User != undefined || this.currentUser.User !== null) {
        this.userId = this.currentUser.User.UserId;
      }
    });
    this._loaderService.show();
    this.vendorId = this._route.snapshot.paramMap.get('id');
    this.getVendorById();
    this.setDate();
    this.setDueDate();
    this.getInvoiceList();
  }

  ngOnDestroy() {
    this.user$.unsubscribe();
  }

  multiSelectDefault() {
    setTimeout(() => {
      const selectedElement: Element = document.querySelectorAll('ng2-smart-table table')
        .item(0);

      if (selectedElement) {
        const row: HTMLElement = selectedElement.querySelector('th') as HTMLElement;
        row.click();
        this.selectedRow = this.smartTable.grid.getSelectedRows().length;
      }
    }, 200);
  }
  setDate() {
    this.model = {
      year: this.now.getFullYear(), month: this.now.getMonth() + 1,
      day: this.firstDay(this.now.getFullYear(), this.now.getMonth())
    };
    this.startOrderDate = this.model;
    this.modelend = {
      year: this.now.getFullYear(), month: this.now.getMonth() + 1,
      day: this.lastDay(this.now.getFullYear(), this.now.getMonth())
    };
    this.endOrderDate = this.modelend;
  }

  setDueDate() {
    const date = new Date();
    date.setDate(date.getDate() + 10);
    this.modeldue = {
      year: date.getFullYear(), month: date.getMonth() + 1,
      day: date.getDate()
    };
    this.dueDate = this.modeldue;
  }
  getVendorById() {
    this._loaderService.show();
    this._vendorService.getVendorByID(this.vendorId).subscribe(
      data => {
        if (data) {
          this.vendorModal = data;
          this._loaderService.hide();
        }
        this._loaderService.hide();
      }, error => {
        this._loaderService.hide();
      });
  }
  lastDay(year, month) {
    return new Date(year, month + 1, 0).getDate();
  }
  firstDay(year, month) {
    return new Date(year, month + 1, 1).getDate();
  }
  onCancel(form: NgForm) {
    ///  form.reset();
    this._router.navigate(['./invoice/invoice-list']);
  }
  getInvoiceList() {
    this._loaderService.show();
    this.invoiceOrderReq.VendorID = this.vendorId;
    this.invoiceOrderReq.FromDate = this.startOrderDate['year'] + '/' + this.startOrderDate['month'] + '/' + this.startOrderDate['day'];
    this.invoiceOrderReq.ToDate = this.endOrderDate['year'] + '/' + this.endOrderDate['month'] + '/' + this.endOrderDate['day'];
    this._invoiceService.getInvoiceOrder(this.invoiceOrderReq).subscribe(
      data => {
        this._loaderService.hide();
        if (data) {
          this.invoiceOrderList = data;
          this.source = new LocalDataSource(this.invoiceOrderList);
          this.invoiceOrderList.forEach((ele) => {
            if (ele.Price != null || ele.Price != undefined) {
              this.currentTotal = this.currentTotal + Number(ele.Price);
            }
          });
          this.adjustments = 0;
          this.totalPrice = this.currentTotal;
          this.multiSelectDefault();
        }
      }, error => {
        this._loaderService.hide();
      });
  }
  restrictMinus(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
    // const inputKeyCode = e.keyCode ? e.keyCode : e.which;
    // if (inputKeyCode != null) {
    //   if (inputKeyCode == 45) {
    //     e.preventDefault();
    //   }
    // }
  }
  onUserRowSelect(event): void {
    this.currentTotal = 0;
    const val = this.smartTable.grid.getSelectedRows();
    val.forEach((ele) => {
      if (ele.data.Price != null || ele.data.Price != undefined) {
        this.currentTotal = this.currentTotal + Number(ele.data.Price);
      }
    });
    this.totalPrice = this.currentTotal - this.adjustments;
    this.selectedRow = this.smartTable.grid.getSelectedRows().length;
    if (this.selectedRow == this.source.count()) {
      this.checked(true);
    }
    if (this.selectedRow < this.source.count()) {
      this.checked(false);
    }
  }
  checked(Ischecked: boolean) {
    const selectedElement: Element = document.querySelectorAll('ng2-smart-table table')
      .item(0);
    if (selectedElement) {
      const row: HTMLElement = selectedElement.querySelector('th') as HTMLElement;
      row.children[0]['checked'] = Ischecked;
    }
  }

  onAdjustment() {
    this.totalPrice = this.currentTotal - this.adjustments;
  }
  onSubmit(form: NgForm) {
    this.invoiceBody.TimeOffset = this._commonService.getUserTimeZoneOffSet();
    this.invoiceBody.CreatedBy = this.userId;
    this.invoiceBody.VendorID = this.vendorId;
    this.invoiceBody.TotalCost = this.totalPrice;
    this.invoiceBody.ClaimAmount = this.adjustments;
    this.invoiceBody.InvoiceStatusID = 1;
    this.invoiceBody.InvoiceDueDate = this.dueDate['year']
      + '/' + this.dueDate['month']
      + '/' + this.dueDate['day'];
    const selectedRows = this.smartTable.grid.getSelectedRows();
    this.invoiceOrderList = [];
    selectedRows.forEach(element => {
      this.invoiceOrderList.push(element.data);
    });
    this.invoiceBody.InvoiceOrders = this.invoiceOrderList;
    this._loaderService.show();
    this._invoiceService.addInvoice(this.invoiceBody).subscribe((data) => {
      this._loaderService.hide();
      if (data > 0) {
        this._toasterService.success('Invoice Created Successfully for ' + this.vendorModal.VendorName + '!!');
      } else {
        this._toasterService.error('Invoice not Created Successfully!!');
      }
      this._router.navigate(['./invoice/invoice-list']);
    }, (error) => {
      this._loaderService.hide();
      this._toasterService.error('Server Error Occurred');
    });
  }
  onDate(date, start) {
    if (start) {
      this.startOrderDate = date;
      this.getInvoiceList();
    } else {
      this.endOrderDate = date;
      this.getInvoiceList();
    }
  }
  onReset() {
    this.setDate();
    this.getInvoiceList();
  }
}
