import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddNewUserComponent } from './add-new-user/add-new-user.component';
import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserListComponent, CustomRendererComponent } from './user-list/user-list.component';
import { LoaderComponent } from '../bs-component/components/loader/loader.component';
import { ViewUserComponent } from './view-user/view-user.component';
import { Ng2SmartTableModule } from 'ng2-smart-table';
import { ConfirmDialogComponent } from '../bs-component/components/confirm-dialog/confirm-dialog.component';
import { SharedModule } from '../../shared/modules/shared.module';
import { AlwaysAuthGuard } from '../../shared/guard/auth.guard'; 
import { AdminGuard } from '../../shared/guard/admin.guard';
import { ManagerGuard } from '../../shared/guard/manager.guard';

const routes: Routes = [{
  path: '',
  children: [
    { path: 'add-user', component: AddNewUserComponent },
    { path: 'edit-user/:id', component: AddNewUserComponent },
    { path: 'view-user/:id', component: ViewUserComponent },
    { path: 'user-list', component: UserListComponent },
  ], canActivate: [AlwaysAuthGuard, AdminGuard, ManagerGuard]
}];
@NgModule({
  imports: [
    CommonModule,
    NgbModule.forRoot(),
    RouterModule.forChild(routes),
    // FormsModule,
    SharedModule,
    // ReactiveFormsModule,
    // Ng2SmartTableModule
  ],
  declarations: [AddNewUserComponent, UserListComponent, ViewUserComponent],
  entryComponents: []
})
export class UsersModule { }
