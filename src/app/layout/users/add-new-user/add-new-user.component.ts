import { Component, OnInit, OnDestroy } from '@angular/core';
import { User } from '../../../model/user.model';
import { UserService } from '../../../shared/services/user.service';
import { Location } from '@angular/common';
import { LoaderService } from '../../../shared/services/loader.service';
import { NgForm } from '@angular/forms';
import { routerTransition } from '../../../router.animations';
import { ActivatedRoute, Router } from '@angular/router';
import { Vendor } from '../../../model/vendor.model';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from '../../../shared/services/common.service';
import { SimpleModal } from '../../../model/simple.model';
import { Subscription } from 'rxjs';
import { GuardService } from '../../../shared/services/guard.service';
import { UserLogin } from '../../../model/login-user.model';

@Component({
  selector: 'app-add-new-user',
  templateUrl: './add-new-user.component.html',
  styleUrls: ['./add-new-user.component.scss'],
  animations: [routerTransition()]
})
export class AddNewUserComponent implements OnInit, OnDestroy {
  model: any = {};
  public user: User;
  userTypes: SimpleModal[];
  localPorts: SimpleModal[];
  vendors: Vendor[];
  showVendorFlag: boolean;
  showLocalPortFlag: boolean;
  subBtnName: string;
  topHeader: string;
  emailDisableFlag: boolean;
  user$: Subscription;
  currentUser: UserLogin;
  userId: any;

  constructor(
    private _userService: UserService,
    private _loaderService: LoaderService,
    private _commonService: CommonService,
    private route: ActivatedRoute,
    private router: Router,
    private _guard: GuardService,
    private _toasterService: ToastrService,
    private _location: Location
  ) {
    this.user = new User();
    this.userTypes = [];
    this.localPorts = [];
    this.vendors = [];
    this.showLocalPortFlag = false;
    this.showVendorFlag = false;
    this.subBtnName = 'Add User';
    this.topHeader = 'Add New User';
    this.emailDisableFlag = false;
  }

  ngOnInit() {
    this.user$ = this._guard.user$.subscribe(user => {
      this.currentUser = user;
      if (this.currentUser.User != undefined || this.currentUser.User !== null) {
        this.userId = this.currentUser.User.UserId;
      }
    });
    this._loaderService.show();
    this.getVendors();
    this.getUserTypes();
    this.getLocalPorts();
    this.onPageLoad();
  }
  ngOnDestroy() {
    this.user$.unsubscribe();
  }
  onPageLoad() {
    const id = this.route.snapshot.paramMap.get('id');
    if (id != null) {
      this.subBtnName = 'Update User';
      this.topHeader = 'Edit User';
      this.emailDisableFlag = true;
      this._userService.getUserByID(id).subscribe(
        data => {
          if (data) {
            this.user = data;
            if (this.user.UserTypeID == '4') {
              this.showLocalPortFlag = true;
              this.showVendorFlag = false;
            }
            if (this.user.UserTypeID == '5') {
              this.showVendorFlag = true;
              this.showLocalPortFlag = false;
            }
            this._loaderService.hide();
          }
        }, error => {
          this._loaderService.hide();
          console.log(error);
        });
    } else {
      this.subBtnName = 'Add User';
      this.topHeader = 'Add New User';
      this.emailDisableFlag = false;
    }
  }

  onSubmit(form: NgForm) {
    this._loaderService.show();
    if (this.user.UserId > 0) {
      this.updateUser();
    } else {
      this.addUser(form);
    }
  }

  ChnageUserType(val: any) {
    if (val === '4') {
      this.showLocalPortFlag = true;
      this.showVendorFlag = false;
      // this.user.LocalPortID = '';
    } else if (val === '5') {
      this.showVendorFlag = true;
      this.showLocalPortFlag = false;
      // this.user.VendorID = '';
    } else {
      this.showVendorFlag = false;
      this.showLocalPortFlag = false;
      // this.user.LocalPortID = '';
      // this.user.VendorID = '';
    }
  }

  addUser(form: NgForm) {
    this._loaderService.show();
    this.user.TimeOffSet = this._commonService.getUserTimeZoneOffSet();
    this.user.CreatedBy = this.currentUser.User.UserId;
    this.user.UpdatedBy = this.currentUser.User.UserId;
    this._userService.addUser(this.user).subscribe(
      data => {
        if (data) {
          this._loaderService.hide();
          if (data > 0) {
            this._toasterService.success('User Added Successfully!!');
            form.resetForm();
          } else {
            this._toasterService.error('User Not Added');
          }
        }
      }, error => {
        this._loaderService.hide();
        this._toasterService.error('Server Error Occurred');
      });
  }
  updateUser() {
    this._loaderService.show();
    this.user.TimeOffSet = this._commonService.getUserTimeZoneOffSet();
    this.user.UpdatedBy = this.currentUser.User.UserId;
    this._userService.updateUser(this.user).subscribe(
      data => {
        if (data) {
          this._loaderService.hide();
          if (data === true) {
            this._toasterService.success('User Updated Successfully!!');
            this._location.back();
            // this.router.navigate(['/user/user-list']);
          } else {
            this._toasterService.error('User Not Updated');
          }
        }
      }, error => {
        this._loaderService.hide();
        this._toasterService.error('Server Error Occurred');
      });
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this._loaderService.show();
      const file = event.target.files[0];
      console.log(file);
      const fileReader = new FileReader();
      fileReader.onloadend = (e) => {
        const rawData = fileReader.result;
        this._loaderService.hide();
        this.user.ProfilePicture = rawData.toString();
        // console.log(rawData);
      };
      fileReader.readAsDataURL(file);
    }
  }

  getVendors() {
    this._userService.getActiveVendors().subscribe(
      data => {
        if (data) {
          this._loaderService.hide();
          this.vendors = data;
        }
      }, error => {
        this._loaderService.hide();
        console.log(error);
      });
  }
  getUserTypes() {
    this._userService.getAllUserTypes().subscribe(
      data => {
        if (data) {
          this._loaderService.hide();
          console.log(data);
          this.userTypes = data;
        }
      }, error => {
        this._loaderService.hide();
        console.log(error);
      });
  }
  getLocalPorts() {
    this._userService.getAllLocalPorts().subscribe(
      data => {
        if (data) {
          this._loaderService.hide();
          console.log(data);
          this.localPorts = data;
        }
      }, error => {
        this._loaderService.hide();
        console.log(error);
      });
  }
}
