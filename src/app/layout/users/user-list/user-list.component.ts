import { Component, OnInit, Input, Output, ViewChild, EventEmitter } from '@angular/core';
import { User } from '../../../model/user.model';
import { UserService } from '../../../shared/services/user.service';
import { routerTransition } from '../../../router.animations';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';
import { LoaderService } from '../../../shared/services/loader.service';
import { LocalDataSource, ViewCell } from 'ng2-smart-table';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmDialogComponent } from '../../bs-component/components/confirm-dialog/confirm-dialog.component';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss'],
  animations: [routerTransition()]
})
export class UserListComponent implements OnInit {
  usersList: User[];
  userListCopy: User[];
  selectedUserType = '2';
  source = new LocalDataSource(); // add a property to the component
  settings = {
    actions: false,
    columns: {
      FirstName: {
        title: 'Name',
        type: 'custom',
        valuePrepareFunction: (cell, usersList) => {
          return usersList.FirstName + ' ' + usersList.LastName;
        },
        renderComponent: CustomRendererComponent,
        filter: false
      },
      Email: {
        title: 'Email',
        filter: false
      },
      Phone: {
        title: 'Phone',
        filter: false
      },
      UserId: {
        title: 'Action',
        type: 'custom',
        renderComponent: CustomRendererComponent,
        filter: false
      },
      IsActive: {
        title: 'Block/Unblock',
        type: 'custom',
        filter: false,
        renderComponent: CustomRendererComponent,
        onComponentInitFunction: (instance) => {
          instance.save.subscribe(row => {
            if (row) {
              this.getUsersList();
            }
          });
        }
      }
    },
    pager: {
      display: true,
      perPage: 10,
    },
    hideSubHeader: true,
    sort: true,
  };
  constructor(
    private _userService: UserService,
    private _router: Router,
    private _loaderService: LoaderService,
    private _modalService: NgbModal
  ) {
    this.usersList = [];
    this.userListCopy = [];
  }

  ngOnInit() {
    this.getUsersList();
  }

  getUsersList(): any {
    this._loaderService.show();
    this._userService.getAllInternalUsers().subscribe(
      data => {
        if (data) {
          this.usersList = data;
          this.onSelectUserType();
        }
      }, error => {
        console.log(error);
        this._loaderService.hide();
      });
  }

  onSearch(query: string = '') {
    console.log('query', query);
    if (query) {
      this.source.setFilter([
        // fields we want to include in the search
        {
          field: 'FirstName',
          search: query
        },
        {
          field: 'LastName',
          search: query
        },
        {
          field: 'Email',
          search: query
        },
        {
          field: 'Phone',
          search: query
        }
      ], false, true);
    } else {
      if (this.selectedUserType !== '') {
        this.source = new LocalDataSource(this.userListCopy);
      } else {
        this.source = new LocalDataSource(this.usersList);
      }
    }
  }

  onSelectUserType() {
    this._loaderService.show();
    this.userListCopy = [];
    let flag = false;
    if (this.selectedUserType === '') {
      this.source = new LocalDataSource(this.usersList);
      this._loaderService.hide();
    } else {
      this.usersList.forEach((user, index) => {
        if (user.UserTypeID == this.selectedUserType) {
          this.userListCopy.push(user);
        }
        if (index === this.usersList.length - 1) {
          flag = true;
        }
      });
      if (flag) {
        this.source = new LocalDataSource(this.userListCopy);
        this._loaderService.hide();
      }
    }
  }
}
@Component({
  template: `
    <a class="btn btn-link font-text" (click)="navigateToSomeRoute()">{{title}}</a>
  `,
  styleUrls: ['./user-list.component.scss'],
})
export class CustomRendererComponent implements ViewCell, OnInit {
  value: any;
  rowData: any;
  title: any;
  flagBlock: boolean;
  vendorId: any;
  @Output() save: EventEmitter<any> = new EventEmitter();
  constructor(private router: Router,
    private _modalService: NgbModal,
    private _userService: UserService,
    private _loaderService: LoaderService,
    private _toasterService: ToastrService
  ) {
    this.flagBlock = false;
  }

  renderValue: string;


  ngOnInit() {
    // if (typeof(this.value) === 'object') {
    //   this.vendorId = this.value.vendorId;
    //   this.title = 'View Users'
    // }
    if (typeof (this.value) === 'number') {
      this.title = 'Edit';
    } else if (typeof (this.value) === 'string') {
      this.title = this.value;
    } else if (this.value === true) {
      this.title = 'Block';
      this.flagBlock = true;
    } else if (this.value === false) {
      this.title = 'UnBlock';
      this.flagBlock = false;
    } else {
      this.title = 'Block'; // comment else part if isactive implemented on server side
    }
  }

  navigateToSomeRoute() {
    if (typeof (this.value) === 'number') {
      this.router.navigate(['/user/edit-user/' + this.value]);
    } else if (typeof (this.value) === 'string') {
      this.router.navigate(['/user/view-user/' + this.rowData.UserId]);
    } else {
      const modalRef = this._modalService.open(ConfirmDialogComponent);
      modalRef.componentInstance.titleFrom = 'Confirm Block/Unblock ';
      modalRef.componentInstance.message = 'Are You Sure ? Do you want to ' + this.title + ' '
        + this.rowData.FirstName + ' ' + this.rowData.LastName + ' ?';
      modalRef.result.then((result) => {
        if (result) {
          const id = this.rowData.UserId;
          if (this.flagBlock === true) {
            this.blockUser(id);
          } else {
            this.unblockUser(id);
          }
        }
      });
    }
  }
  blockUser(id: any) {
    this._loaderService.show();
    this._userService.blockUser(id).subscribe(
      data => {
        this._loaderService.hide();
        if (data === true) {
          this.save.emit('true');
          this._toasterService.success(this.rowData.FirstName + ' ' + this.rowData.LastName + ' is Blocked Successfully!');
        } else {
          this._toasterService.error('Server Error Occurred!!');
        }
      }, error => {
        this._loaderService.hide();
        this._toasterService.error('Server Error Occurred!!');
      });
  }
  unblockUser(id: any) {
    this._loaderService.show();
    this._userService.unblockUser(id).subscribe(
      data => {
        this._loaderService.hide();
        if (data === true) {
          this.save.emit('true');
          this._toasterService.success(this.rowData.FirstName + ' ' + this.rowData.LastName + ' is UnBlocked Successfully!');
        } else {
          this._toasterService.error('Server Error Occurred!!');
        }
      }, error => {
        this._loaderService.hide();
        this._toasterService.error('Server Error Occurred!!');
      });
  }
}

