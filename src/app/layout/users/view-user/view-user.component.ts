import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { UserService } from '../../../shared/services/user.service';
import { LoaderService } from '../../../shared/services/loader.service';
import { User } from '../../../model/user.model';
import { routerTransition } from '../../../router.animations';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.scss'],
  animations: [routerTransition()]
})
export class ViewUserComponent implements OnInit {
  userModal: User;
  showVendorFlag: boolean;
  showLocalPortFlag: boolean;
  constructor(
    private route: ActivatedRoute,
    private _userService: UserService,
    private _loaderService: LoaderService,
    private _location: Location
  ) {
    this.userModal = new User();
    this.showLocalPortFlag = false;
    this.showVendorFlag = false;
  }

  ngOnInit() {
    this._loaderService.show();
    this.getUser();
  }
  getUser(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this._userService.getUserByID(id).subscribe(
      data => {
        if (data) {
          this._loaderService.hide();
          this.userModal = data;
          if (this.userModal.UserTypeID == '4') {
            this.showLocalPortFlag = true;
            this.showVendorFlag = false;
          }
          if (this.userModal.UserTypeID == '5') {
            this.showVendorFlag = true;
            this.showLocalPortFlag = false;
          }
        }
      }, error => {
        this._loaderService.hide();
        console.log(error);
      });
  }
  onOk() {
    this._location.back();
  }
}
