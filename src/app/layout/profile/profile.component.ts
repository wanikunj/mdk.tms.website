import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
import { routerTransition } from '../../router.animations';
import { User } from '../../model/user.model';
import { SimpleModal } from '../../model/simple.model';
import { Vendor } from '../../model/vendor.model';
import { UserService } from '../../shared/services/user.service';
import { LoaderService } from '../../shared/services/loader.service';
import { CommonService } from '../../shared/services/common.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
  animations: [routerTransition()]

})
export class ProfileComponent implements OnInit {

  model: any = {};
  public user: User;
  userTypes: SimpleModal[];
  imgPath = '';
  localPorts: SimpleModal[];
  vendors: Vendor[];
  showVendorFlag: boolean;
  showLocalPortFlag: boolean;
  subBtnName: string;
  topHeader: string;
  emailDisableFlag: boolean;
  userId: any;
  constructor(private route: ActivatedRoute, private _userService: UserService,
    private _loaderService: LoaderService,
    private _commonService: CommonService,
    private router: Router,
    private _toasterService: ToastrService) {
    this.user = new User();
    this.userTypes = [];
    this.localPorts = [];
    this.vendors = [];
    this.showLocalPortFlag = false;
    this.showVendorFlag = false;
    this.subBtnName = 'Update';
    this.topHeader = 'Update Profile';
  }

  ngOnInit() {
    this.userId = this.route.snapshot.paramMap.get('id');
    this._loaderService.show();
    this.getVendors();
    this.getUserTypes();
    this.getLocalPorts();
    this.onPageLoad();
  }

  onPageLoad() {

    this._userService.getUserByID(this.userId).subscribe(
      data => {
        if (data) {
          this.user = data;
          this.imgPath = this.user.ProfilePicture;
          if (this.user.UserTypeID == '4') {
            this.showLocalPortFlag = true;
            this.showVendorFlag = false;
          }
          if (this.user.UserTypeID == '5') {
            this.showVendorFlag = true;
            this.showLocalPortFlag = false;
          }
          this._loaderService.hide();
        }
      }, error => {
        this._loaderService.hide();
        console.log(error);
      });
  }

  onSubmit(form: NgForm) {
    this._loaderService.show();
    this.updateUser();
  }
  updateUser() {
    this._loaderService.show();
    this.user.UpdatedBy = this.user.UserId;
    this.user.TimeOffSet = this._commonService.getUserTimeZoneOffSet();
    this._userService.updateUser(this.user).subscribe(
      data => {
        if (data) {
          this._loaderService.hide();
          if (data === true) {
            this._toasterService.success('Profile Updated Successfully!!');
            this.router.navigate(['/dashboard']);
          } else {
            this._toasterService.error('Profile Not Updated');
          }
        }
      }, error => {
        this._loaderService.hide();
        this._toasterService.error('Server Error Occurred');
      });
  }

  onSelectFile(event) {
    if (event.target.files && event.target.files[0]) {
      this._loaderService.show();
      const file = event.target.files[0];
      console.log(file);
      const fileReader = new FileReader();
      fileReader.onloadend = (e) => {
        const rawData = fileReader.result;
        this._loaderService.hide();
        this.user.ProfilePicture = rawData.toString();
        this.imgPath = this.user.ProfilePicture;
        // console.log(rawData);
      };
      fileReader.readAsDataURL(file);
    }
  }

  getVendors() {
    this._userService.getActiveVendors().subscribe(
      data => {
        if (data) {
          this._loaderService.hide();
          this.vendors = data;
        }
      }, error => {
        this._loaderService.hide();
        console.log(error);
      });
  }
  getUserTypes() {
    this._userService.getAllUserTypes().subscribe(
      data => {
        if (data) {
          this._loaderService.hide();
          console.log(data);
          this.userTypes = data;
        }
      }, error => {
        this._loaderService.hide();
        console.log(error);
      });
  }
  getLocalPorts() {
    this._userService.getAllLocalPorts().subscribe(
      data => {
        if (data) {
          this._loaderService.hide();
          console.log(data);
          this.localPorts = data;
        }
      }, error => {
        this._loaderService.hide();
        console.log(error);
      });
  }
}
