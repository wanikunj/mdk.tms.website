import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import { RouterModule, Router, Routes } from '@angular/router';
import { SharedModule } from '../../shared/modules/shared.module';
import { AlwaysAuthGuard } from '../../shared/guard/auth.guard';

const routes: Routes = [
  {
    path: '', component: ProfileComponent,
    canActivate: [AlwaysAuthGuard]
  }
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ProfileComponent]
})
export class ProfileModule { }
