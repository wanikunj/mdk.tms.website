export class InvoiceOrderReq {
    FromDate: any;
    ToDate: any;
    VendorID: any;
}

export class InvoiceOrders {
    ID: any;
    OrderID: any;
    VehicleName: any;
    Chasis: any;
    AuctionName: any;
    OrderDate: any;
    DeliveryDate: any;
    Price: any;
}

export class Invoice {
    InvoiceID: any;
    VendorID: any;
    TotalCost: any;
    AmountPaid: any;
    AmountDue: any;
    InvoiceDateTime: any;
    PaymentDate: any;
    InvoiceDueDate: any;
    InvoiceStatusName: any;
    InvoiceStatusID: any;
    ClaimAmount: any;
    CreatedBy: any;
    OverDueStatus: any;
    TimeOffset: any;
    InvoiceOrders: InvoiceOrders[];
    constructor() {
        this.TimeOffset = '';
    }
}
export class UpdateInvoiceReq {
    InvoiceID: any;
    StatusID: any;
    Amount: any;
    TimeOffset: any;
    UserID: any;
    constructor() {
        this.TimeOffset = '';
    }
}
