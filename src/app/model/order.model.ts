export class Order {
    CustomerName: string;
    CompanyNumber: string;
    CompanyMobile: string;
    CompanyFax: string;
    CompanyAddress: string;
    AuctionName: string;
    AuctionMembershipNumber: string;
    LotNumber: string;
    CarName: string;
    ChassisNumber: string;
    TrackingDigit: string;
    Destination: string;
    ExportDestination: string;
    Location: string;
    POSNumber: string;
    PlateNumber: string;
    Remarks: string;
    // BuyingDate:Date;
    // DeliveryDateTime:Date;
    // OrderDetailId:number;
    // OrderNo:number;
    // OrderStatusId:number;
    // OrderTypeId:number;
    // StatusName:string;
    // isActive:boolean
    // OrderCreatedDate:Date;
    // OrderUpdatedDate:Date;
}

export class FilterObj {
    VendorID: any;
    OrderDate: any;
    AuctionID: any;
    constructor() {
        this.VendorID = '';
        this.OrderDate = '';
        this.AuctionID = '';
    }
}
