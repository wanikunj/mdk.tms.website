export class User {

    UserId: number;
    FirstName: string;
    LastName: string;
    Email: string;
    Password: string;
    UserTypeID: string;
    UserType: string;
    LocalPortID: string;
    LocalPort: string;
    VendorID: string;
    Vendor: string;
    UserDetailId: number;
    UpdatedBy: any;
    CreatedBy: any;
    Phone: string;
    AlternatePhone: string;
    PostalCode: string;
    Province: string;
    City: string;
    Street: string;
    FaxNumber: string;
    ProfilePicture: string;
    IsActive: Boolean;
    TimeOffSet: number;
    constructor() {
        this.UserTypeID = '';
    }
}
