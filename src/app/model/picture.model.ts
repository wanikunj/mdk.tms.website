export class PictureModel {
    PictureID: any;
    OrderID: any;
    PicturePath: any;
    PictureComment: any;
    UserName: any;
    IsActive: any;
    CreatedBy: any;
    UpdatedBy: any;
    CreatedDate: any;
    UpdatedDate: any;
    TimeOffset: any;
    VehicleName: any;
    Chasis: any;
}
export class VehicleModel {
    VehicleName: any;
    Chasis: any;
}
