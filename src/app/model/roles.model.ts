export class Roles {
    RoleId: number;
    RoleName: string;
    IsActive: boolean;
    CreatedBy:string;
    UpdatedBy:string;
}
