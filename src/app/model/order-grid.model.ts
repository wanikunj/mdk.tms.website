export class OrderList {
    OrderId: Number;
    VendorName: string;
    OrderStatus: string;
    AuctionName: string;
    OrderDate: Date;
    IsActive: boolean;
    VehicleName: string;
    VehicleTypeID: any;
    PaymentStatus: string;
    OrderStatusID: any;
    UpdateDate: any;
    UpdateBy: any;
    TimeOffSet: any;
    constructor() {
        // this.OrderId = '';
        this.VehicleName = '';
        this.VendorName = '';
        this.TimeOffSet = 0;
        this.PaymentStatus = '';
        this.OrderStatus = '';
        this.OrderStatusID = '';
        this.AuctionName = '';
    }
}
