import { Vendor } from './vendor.model';

export class DriverFilterObj {
    FilterName: string;
    TimeOffSet: any;
    UserID: any;
    constructor() {
        this.FilterName = 'All';
    }
}

export class OrderDetail extends Vendor {
    OrderId: any;
    AuctionName: any;
    OrderDate: any;
    Lot: any;
    CarName: any;
    Chasis: any;
    PickupLoc: any;
    DestLoc: any;
    MembershipNumber: any;
    Vendor: Vendor;
    OrderDetailID: any;
    OrderStatusID: any;
    VehicleTypeID: any;
    VehicleTypeName: any;
    TruckCategoryID: any;
    TruckCategoryName: any;
    TruckWeightID: any;
    TruckWeightName: any;
    SpecialType: any;
    ConditionID: any;
    ConditionName: any;
    ConditionDetails: any;
    Price: any;
    Message: any;
    TransportMode: any;
    Driver: any;
    DriverName: any;
    UpdatedBy: any;
    UpdatedDate: string;
    TimeOffSet: any;
    VendorName: string;
    constructor(value?) {
        super();
        if (value) {

            this.AuctionName = value;
            this.OrderDate = value;
            this.Lot = value;
            this.CarName = value;
            this.Chasis = value;
            this.PickupLoc = value;
            this.DestLoc = value;
            this.VehicleTypeName = value;
            this.UpdatedDate = value;
            this.VendorName = value;
        }
    }
}

