export class VendorOrder {
    OrderId: number;
    VendorID: any;
    AuctionID: string;
    MembershipNumber: string;
    AuctionDate: any;
    AuctionDateValue: string;
    OrderDetails: VendorOrderDetail[] = [];
    TimeOffSet: number;
    CreatedBy: number;
    UpdatedBy: number;
    constructor() {
        this.MembershipNumber = '';
       // this.VendorID = 1;
       // this.OrderId = 0;
        this.AuctionID = '';
    }
}

export class Auction {
    AuctionID: any;
    AuctionName: any;
    AuctionHouseAddress: any;
}

export class VendorOrderDetail {
    LotNumber: any;
    CarName: any;
    ChassisNumber: any;
    Dest_AuctionID: any;
    Dest_MDKGarageID: any;
    Dest_LocalPortID: any;
    OrderStatusID: any;
    constructor() {
        this.OrderStatusID = 'Order Pending';
        this.LotNumber = '';
        this.CarName = '';
        this.ChassisNumber = '';
        this.Dest_LocalPortID = 1;
    }
}
export class DestinationLocationId {
    index: any;
    value: any;
}

export class LocalPort {
    ID: number;
    Value: any;
}

export class MDKGarage {
    MDKGarageID: number;
    MDKGarageName: any;
    MDKGarageAddress: any;
}

export class HelperClass {
    ID: number;
    Value: any;
}
