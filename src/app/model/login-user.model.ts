
export class UserModel {
    UserId: any;
    FirstName: string;
    LastName: string;
    Email: string;
    Password: string;
    UserTypeID: any;
    UserType: string;
    LocalPortID: any;
    LocalPort: any;
    VendorID: any;
    Vendor: any;
    UserDetailId: any;
    Phone: any;
    AlternatePhone: any;
    PostalCode: any;
    Province: any;
    City: any;
    Street: any;
    FaxNumber: any;
    ProfilePicture: any;
    IsActive: any;
    TimeOffSet: any;
    CreatedBy: any;
    UpdatedBy: any;
    CreatedDate: any;
    UpdatedDate: any;
    constructor() {
        this.UserType = '';
    }
}
export class UserLogin {
    UserID: any;
    User: UserModel;
    Email: any;
    Password: any;
    Token: any;
    TimeOffset: any;
    IsTokenActive: any;
    LocalPorts: any;
    Vendors: any;
    isAdmin: boolean;
    isDriver: boolean;
    isLocalPortEmployee: boolean;
    isManager: boolean;
    isVendor: boolean;
    constructor() {
       // super();
        this.Email = '';
        this.Password = '';
        this.isAdmin = false;
        this.isDriver = false;
        this.isLocalPortEmployee = false;
        this.isManager = false;
        this.isVendor = false;
        this.User = new UserModel();
    }
}

export class LoginModal {
    UserID: any;
    Email: any;
    Password: any;
    Token: any;
    TimeOffset: any;
}
