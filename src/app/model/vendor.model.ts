import { User } from './user.model';

export class Vendor {

    VendorID: number;
    VendorDetailsID: number;
    VendorShortName: string;
    VendorName: string;
    VendorPhone: string;
    VendorCell: string;
    VendorFax: string;
    PostalCode: string;
    Province: string;
    City: string;
    Street: string;
    VendorLogo: string;
    PaymentScheduleID: string;
    PaymentSchedule: string;
    Users: User[];
    IsActive: any;
    TimeOffSet: number;
    CreatedBy: string;
    UpdatedBy: string;
    CreatedDate: string;
    UpdatedDate: string;
    constructor() {
        this.PaymentScheduleID = '';
        this.VendorName = '';
        this.VendorPhone = '';
        this.Street = '';
        this.Province = '';
        this.PostalCode = '';
        this.City = '';
        this.VendorFax = '';
    }
}
