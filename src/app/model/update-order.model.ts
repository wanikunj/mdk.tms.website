import { Vendor } from './vendor.model';

export class UpdateOrderDetails extends Vendor {
    OrderId: any;
    OrderPicsCount: any;
    OrderCommentsCount: any;
    AuctionName: string;
    OrderDate: any;
    Lot: any;
    CarName: string;
    Chasis: string;
    PickupLoc: string;
    DestLoc: string;
    MembershipNumber: string;
    Vendor: Vendor;


    OrderDetailID: any;
    OrderStatusID: any;
    VehicleTypeID: any;
    TruckCategoryID: any;
    TruckWeightID: any;
    SpecialType: string;
    ConditionID: any;
    ConditionDetails: string;
    Price: string;
    Message: string;
    TransportMode: string;
    Driver: any;
    UpdatedBy: any;
    UpdatedDate: any;
    TimeOffSet: any;
    constructor() {
        super();
        this.OrderStatusID = 1;
        this.VehicleTypeID = 1;
        this.ConditionID = 1;
        this.TruckCategoryID = 0;
        this.TruckWeightID = 0;
        this.ConditionDetails = '';
        this.SpecialType = '';
        this.Price = '';
        this.Message = '';
        this.TransportMode = '';
        this.Driver = 0;
    }
}
