import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ApiService } from './shared/services/api.service';
import { OrderService } from './shared/services/order.service';
import { UserService } from './shared/services/user.service';
import { LoaderService } from './shared/services/loader.service';
import { SharedModule } from './shared/modules/shared.module';
import { CommonService } from './shared/services/common.service';
import { AdminGuard } from './shared/guard/admin.guard';
import { LocalPortGuard } from './shared/guard/local-port.guard';
import { DriverGuard } from './shared/guard/driver.guard';
import { ManagerGuard } from './shared/guard/manager.guard';
import { AlwaysAuthGuard } from './shared';
import { CookieService } from 'ngx-cookie-service';
import { VendorGuard } from './shared/guard/vendor.guard';

// AoT requires an exported function for factories
export const createTranslateLoader = (http: HttpClient) => {
    /* for development
    return new TranslateHttpLoader(
        http,
        '/start-angular/SB-Admin-BS4-Angular-6/master/dist/assets/i18n/',
        '.json'
    ); */
    return new TranslateHttpLoader(http, './assets/i18n/', '.json');
};

@NgModule({
    imports: [
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        HttpClientModule,
        SharedModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: createTranslateLoader,
                deps: [HttpClient]
            }
        }),
        AppRoutingModule
    ],
    declarations: [AppComponent],
    providers: [LoaderService, AdminGuard, LocalPortGuard, DriverGuard, ManagerGuard, VendorGuard, AlwaysAuthGuard, CookieService],

    bootstrap: [AppComponent]
})
export class AppModule { }
