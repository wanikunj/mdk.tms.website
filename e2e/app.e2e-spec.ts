import { AppPage } from './app.po';

describe('TMSWEB App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Transport Management System');
  });
});
